pub mod mock_objects;

use std::{
    net::Ipv4Addr,
    sync::{Arc, Mutex},
};

use hyper::{header, Method};
use icinga_client::{
    client::{Api, Client, RequestBuilder},
    types::{CheckedObject, Host, IcingaObjectResults, Service},
};
use serde::de::DeserializeOwned;
use serde_json::Value;
use tokio::runtime::Runtime;

#[derive(Debug, Clone)]
pub struct IcingaOptions {
    pub address: Ipv4Addr,
    pub port: u16,
    pub credentials: Option<(String, String)>,
}

impl IcingaOptions {
    pub fn new() -> Self {
        Self::from_address("127.0.0.1".parse().unwrap())
    }
    pub fn from_address(address: Ipv4Addr) -> Self {
        Self::from_address_and_port(address, 0)
    }

    pub fn from_address_and_port(address: Ipv4Addr, port: u16) -> Self {
        IcingaOptions {
            address,
            port,
            credentials: None,
        }
    }

    pub fn with_credentials<S: ToString, T: ToString>(
        &mut self,
        user: S,
        password: T,
    ) -> &mut Self {
        self.credentials = Some((user.to_string(), password.to_string()));
        self
    }
}

#[derive(Debug, Clone)]
pub struct State(Arc<Mutex<(Vec<Host>, Vec<Service>)>>);

impl State {
    pub fn new(hosts: Vec<Host>, services: Vec<Service>) -> Self {
        State(Arc::new(Mutex::new((hosts, services))))
    }

    pub fn with_hosts<R>(&self, f: impl FnOnce(&mut Vec<Host>) -> R) -> R {
        f(&mut self.0.lock().unwrap().0)
    }

    pub fn with_services<R>(&self, f: impl FnOnce(&mut Vec<Service>) -> R) -> R {
        f(&mut self.0.lock().unwrap().1)
    }
}

pub struct Session(Runtime, Client);

impl Session {
    pub fn new() -> Self {
        let options = IcingaOptions::new();
        Self::from_options(&options)
    }

    pub fn from_options(options: &IcingaOptions) -> Self {
        let state = State::new(mock_objects::hosts(), mock_objects::services());
        let rt = Runtime::new().unwrap();
        let client = start_server(options, state, &rt);
        Self(rt, client)
    }

    pub fn get_objects<T: DeserializeOwned + CheckedObject>(&self) -> Vec<T> {
        self.get::<IcingaObjectResults<T>>(T::PATH)
            .results
            .into_iter()
            .map(|r| r.attrs)
            .collect::<Vec<_>>()
    }

    pub fn post<T: DeserializeOwned>(&self, path: &str, query: &[(&str, &str)], body: Value) -> T {
        self.request(Method::POST, path, |r| {
            r.query(query)
                .header(header::CONTENT_TYPE, "application/json")
                .body(body.to_string())
        })
    }

    pub fn get<T: DeserializeOwned>(&self, path: &str) -> T {
        self.request(Method::GET, path, |r| r)
    }

    pub fn request<T: DeserializeOwned>(
        &self,
        method: Method,
        path: &str,
        make_request: impl FnOnce(RequestBuilder) -> RequestBuilder,
    ) -> T {
        self.with_client(|client| {
            let request = make_request(client.request(method, path).unwrap());
            client.send_request(request).unwrap()
        })
    }

    pub fn with_client<R>(&self, f: impl FnOnce(&Client) -> R) -> R {
        let Self(_rt, client) = self;
        f(&client)
    }
}

fn start_server(options: &IcingaOptions, state: State, rt: &Runtime) -> Client {
    let options = options.clone();
    let h = rt.spawn(async move {
        let server = server::server(&options, state).await.unwrap();
        let addr = server.local_addr();
        tokio::spawn(server);
        addr
    });
    let addr = rt.block_on(h).unwrap();
    Client::new(
        format!("http://{}", addr).parse().unwrap(),
        Api::Icinga,
        None,
        None,
    )
    .unwrap()
}

pub mod server {
    use axum::{
        error_handling::HandleErrorLayer,
        extract::{Extension, Query},
        routing::{get, post, IntoMakeService},
        BoxError, Json, Router,
    };

    use hyper::{server::conn::AddrIncoming, StatusCode};
    use icinga_client::types::{
        AckState, Acknowledgement, CheckedObject, IcingaObjectResult, IcingaObjectResults,
    };
    use serde::{Deserialize, Serialize};
    use serde_json::json;
    use std::net::SocketAddr;
    use tower::ServiceBuilder;
    use tower_http::auth::RequireAuthorizationLayer;

    use crate::{mock_objects, IcingaOptions, State};

    pub type Server = axum::Server<AddrIncoming, IntoMakeService<Router>>;

    #[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
    #[serde(untagged)]
    enum Selector {
        Host(HostSelector),
        Service(ServiceSelector),
    }

    #[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
    struct HostSelector {
        host: String,
    }

    #[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
    struct ServiceSelector {
        service: String,
    }

    #[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
    struct AckBody {
        expiry: Option<f64>,
    }

    pub async fn server(opts: &IcingaOptions, state: State) -> hyper::Result<Server> {
        // build our application with a route
        let addr = SocketAddr::from((opts.address, opts.port));
        let services = ServiceBuilder::new()
            .layer(Extension(state))
            .layer(HandleErrorLayer::new(handle_unexpected_error))
            .option_layer(opts.credentials.as_ref().map(|(username, password)| {
                RequireAuthorizationLayer::basic(&username, &password)
            }));
        let app = Router::new()
            .route("/", get(|| async { "Welcome to icinga-mock" }))
            .route(
                "/v1/objects/hosts",
                get(|Extension(state): Extension<State>| async move {
                    Json(state.with_hosts(|hosts| results(hosts.clone())))
                }),
            )
            .route(
                "/v1/objects/services",
                get(|Extension(state): Extension<State>| async move {
                    Json(state.with_services(|services| results(services.clone())))
                }),
            )
            .route(
                "/v1/actions/acknowledge-problem",
                post(
                    |Query(selector): Query<Selector>,
                     Json(body): Json<AckBody>,
                     Extension(state): Extension<State>| async move {
                        fn ack<T: CheckedObject>(
                            objects: &mut Vec<T>,
                            selected_name: &str,
                            ack_state: &AckState,
                        ) {
                            for obj in objects {
                                if obj.name() == selected_name {
                                    obj.set_acknowledgement(Acknowledgement {
                                        state: ack_state.clone(),
                                        last_change: None,
                                    });
                                    obj.set_handled(true);
                                }
                            }
                        }
                        let ack_state = body
                            .expiry
                            .map(mock_objects::ack_with_expiry)
                            .unwrap_or(mock_objects::ack_without_expiry());
                        match selector {
                            Selector::Host(HostSelector { host: name }) => {
                                state.with_hosts(|hosts| ack(hosts, &name, &ack_state))
                            }
                            Selector::Service(ServiceSelector { service: name }) => {
                                state.with_services(|services| ack(services, &name, &ack_state))
                            }
                        }
                        Json(json!({
                           "results": [
                               {
                                   "code": 200i32,
                                   "status": "Successfully acknowledged problem ..."
                               }
                           ]
                        }))
                    },
                ),
            )
            .layer(services);
        Ok(axum::Server::try_bind(&addr)?.serve(app.into_make_service()))
    }

    async fn handle_unexpected_error(err: BoxError) -> (StatusCode, String) {
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Unexpected error in middleware: {}", err),
        )
    }

    fn results<T: Serialize>(os: Vec<T>) -> IcingaObjectResults<T> {
        IcingaObjectResults {
            results: os
                .into_iter()
                .map(|o| IcingaObjectResult { attrs: o })
                .collect(),
        }
    }

    #[cfg(test)]
    mod test {
        use super::*;

        #[test]
        fn test_de_selector_host() {
            let v = json!({"host": "host2"});

            assert_eq!(
                serde_json::from_value::<Selector>(v).unwrap(),
                Selector::Host(HostSelector {
                    host: "host2".to_string()
                })
            );
        }

        #[test]
        fn test_de_selector_service() {
            let v = json!({"service": "service2"});

            assert_eq!(
                serde_json::from_value::<Selector>(v).unwrap(),
                Selector::Service(ServiceSelector {
                    service: "service2".to_string()
                })
            );
        }
    }
}
