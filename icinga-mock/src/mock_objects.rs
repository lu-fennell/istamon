use chrono::Utc;
use icinga_client::types::{
    AckKind, AckState, Acknowledgement, Host, HostState, Service, ServiceState, Timestamp,
};

pub fn hosts() -> Vec<Host> {
    vec![
        host(1, HostState::UP, ack_none()),
        host(2, HostState::DOWN, ack_none()),
        host(3, HostState::DOWN, ack_with_default_expiry()),
        host(4, HostState::DOWN, ack_without_expiry()),
    ]
}

pub fn hosts2() -> Vec<Host> {
    vec![
        host(10, HostState::DOWN, ack_none()),
        host(20, HostState::UP, ack_none()),
        host(30, HostState::DOWN, ack_with_default_expiry()),
        host(40, HostState::DOWN, ack_without_expiry()),
    ]
}

pub fn services() -> Vec<Service> {
    vec![
        service(1, 1, ServiceState::OK, ack_with_default_expiry()),
        service(2, 1, ServiceState::WARNING, ack_none()),
        service(3, 1, ServiceState::CRITICAL, ack_without_expiry()),
        service(4, 1, ServiceState::UNKNOWN, ack_none()),
    ]
}

pub fn services2() -> Vec<Service> {
    vec![
        service(10, 10, ServiceState::WARNING, ack_with_default_expiry()),
        service(20, 10, ServiceState::WARNING, ack_with_default_expiry()),
        service(30, 10, ServiceState::CRITICAL, ack_without_expiry()),
        service(40, 10, ServiceState::UNKNOWN, ack_none()),
    ]
}

fn host(i: usize, state: HostState, ack: AckState) -> Host {
    let name = format!("host{}", i);
    let handled = ack != AckState::None;
    let now = Utc::now().timestamp();
    let acknowledgement = Acknowledgement {
        state: ack,
        last_change: None,
    };
    Host {
        name: name.clone(),
        display_name: name.clone(),
        address: format!("127.0.0.{}", i),
        address6: format!("::{}", i),
        state,
        last_state: state,
        last_hard_state: state,
        last_state_change: ts(now),
        last_state_up: ts(now - 5),
        last_state_down: ts(now - 60),
        next_check: ts(now + 60),
        last_check_result: None,
        acknowledgement,
        handled,
    }
}

fn service(i: usize, host_id: usize, state: ServiceState, ack: AckState) -> Service {
    let name = format!("service{}", i);
    let host_name = format!("host{}", host_id);
    let handled = ack != AckState::None;
    let now = Utc::now().timestamp();
    let acknowledgement = Acknowledgement {
        state: ack,
        last_change: None,
    };
    Service {
        name: name.clone(),
        display_name: name.clone(),
        host_name,
        state,
        last_state: state,
        last_hard_state: state,
        last_state_change: ts(now),
        last_state_ok: ts(now - 5),
        last_state_warning: ts(now - 60),
        last_state_critical: ts(now - 120),
        last_state_unknown: ts(now - 180),
        next_check: ts(now + 60),
        last_check_result: None,
        last_reachable: true,
        acknowledgement,
        handled,
    }
}

fn ack_none() -> AckState {
    AckState::None
}

pub fn ack_with_expiry(t: f64) -> AckState {
    AckState::Acknowledged {
        kind: AckKind::Normal,
        expiry: Some(Timestamp::new(t)),
    }
}

fn ack_with_default_expiry() -> AckState {
    ack_with_expiry((Utc::now().timestamp() + 60) as f64)
}

pub fn ack_without_expiry() -> AckState {
    AckState::Acknowledged {
        kind: AckKind::Normal,
        expiry: None,
    }
}

fn ts(t: i64) -> Timestamp {
    Timestamp::new(t as f64)
}
