use std::{net::Ipv4Addr, time::Duration};

use icinga_client::types::{Host, HostState, Service, ServiceState};
use icinga_mock::{mock_objects, IcingaOptions, State};
use structopt::StructOpt;
use tokio::task::JoinHandle;
use tracing::level_filters::LevelFilter;
use tracing_subscriber::EnvFilter;

#[derive(StructOpt)]
struct Opts {
    #[structopt(long, short, parse(try_from_str), default_value = "127.0.0.1")]
    address: Ipv4Addr,
    #[structopt(long, short, default_value = "5665")]
    port: u16,
    #[structopt(long, default_value = "test-1")]
    configuration_name: String,
}

impl From<Opts> for IcingaOptions {
    fn from(opts: Opts) -> Self {
        IcingaOptions::from_address_and_port(opts.address, opts.port)
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opts = Opts::from_args();
    // initialize tracing
    tracing_subscriber::fmt::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .init();
    let state = match opts.configuration_name.as_str() {
        "test-1" => State::new(mock_objects::hosts(), mock_objects::services()),
        "test-2" => State::new(mock_objects::hosts2(), mock_objects::services2()),
        s => return Err(format!("Unknown state name {s}").into()),
    };
    // don't change the states for now (cf issue #70)
    // spawn_state_changes_job(state.clone());

    let server = icinga_mock::server::server(&opts.into(), state).await?;
    tracing::info!("listening on {}", server.local_addr());
    server.await?;
    Ok(())
}

// not used for now (cf issue #70)
#[allow(unused)]
fn spawn_state_changes_job(state: State) -> JoinHandle<()> {
    tokio::spawn(async move {
        loop {
            tokio::time::sleep(Duration::from_secs(15)).await;
            state.with_hosts(|hs| hosts_step(hs));
            state.with_services(|ss| services_step(ss));
        }
    })
}

fn hosts_step(hs: &mut [Host]) {
    hs.last_mut().map(|h| {
        *h = host_step(h);
    });
}

fn services_step(ss: &mut [Service]) {
    ss.last_mut().map(|s| *s = service_step(s));
}

fn host_step(h: &Host) -> Host {
    let state = match h.state {
        HostState::UP => HostState::DOWN,
        HostState::DOWN => HostState::UP,
    };
    Host {
        state,
        last_state: state.clone(),
        last_hard_state: state.clone(),
        ..h.clone()
    }
}

fn service_step(s: &Service) -> Service {
    let state = match s.state {
        ServiceState::OK => ServiceState::WARNING,
        ServiceState::WARNING => ServiceState::CRITICAL,
        ServiceState::CRITICAL => ServiceState::UNKNOWN,
        ServiceState::UNKNOWN => ServiceState::OK,
    };
    Service { state, ..s.clone() }
}
