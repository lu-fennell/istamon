# icinga-mock

A server mocking icinga responses for testing. It can be used as a library or
as a standalone application. It's functionality is still very limited.

## Run

From the repository root, run:

```
cargo run --bin icinga-mock -- <options>
```

Listen address and port are configurable through command-line options. See `cargo run --bin icinga-mock -- --help`.

Currently two configurations of static (unchanging) host and service states are returned (i.e., two sets of host and service results). They are named `"test-1"` and `"test-2"`. The default is `"test-1"`.  Use the command-line option `--configuration-name test-2` to use the other. 
