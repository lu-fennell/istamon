use core::ffi;
use std::{ffi::CStr, ptr};

use qmetaobject::{PropertyType, QString};

pub use self::wrapper::QWindow;

cpp! {{
    #include<QtQml/qqml.h>
    #include<QtGui/QWindow>
    #include <QtCore/QMetaType>
}}

mod wrapper {
    use super::*;

    cpp! {{
        #include<QtQml/qqml.h>
    }}

    pub struct QWindow {
        self_: *const ffi::c_void,
    }

    impl QWindow {
        /// SAFETY: p needs to be a valid `QWindow*`, or `null`
        pub(super) unsafe fn from_ptr(p: *const ffi::c_void) -> Self {
            QWindow { self_: p }
        }

        pub(super) fn null() -> Self {
            QWindow { self_: ptr::null() }
        }

        pub fn set_icon<T: Into<QString>>(&self, path: T) {
            let p = self.self_;
            let path: QString = path.into();
            // SAFETY: self_ is a "QWindow *" by the safety requirements for [QWindow::from_ptr]
            cpp![unsafe [p as "QWindow *", path as "QString"] {
                p->setIcon(QIcon(path));
            }]
        }
    }
}

cpp_class!(unsafe struct QWindowPointerImpl as "QPointer<QWindow>");

impl QWindowPointerImpl {
    fn cpp_ptr(&self) -> *mut ffi::c_void {
        cpp!(unsafe [self as "QPointer<QWindow> *"] -> *mut ffi::c_void as "QWindow *" {
            return self->data();
        })
    }
}

/// A [QPointer]-like smart pointer to a `QWindow`.
//
// Invariant: if `qpointer` is valid in Qt's `QPointer` sense (i.e., not null), then `qpointer` and
// `qwindow_buffer` point to the same object and that object is a `QWindow`.
pub struct QWindowPointer {
    qpointer: QWindowPointerImpl,
    qwindow_buffer: QWindow,
}

impl QWindowPointer {
    /// Create a [QWindowPointer] from a raw cpp pointer.
    ///
    /// SAFETY: `p` needs to point to a valid `QWindow` pointer, or `null`.
    pub unsafe fn from_ptr(p: *const ffi::c_void) -> Self {
        let qpointer = cpp!(unsafe [p as "QWindow *"] -> QWindowPointerImpl  as "QPointer<QWindow>" {
            return p; // implicit constructor
        });
        let qwindow_buffer = QWindow::from_ptr(qpointer.cpp_ptr());
        Self {
            qpointer,
            qwindow_buffer,
        }
    }

    /// Returns a reference to the [QWindow], or None if it was deleted
    pub fn as_ref(&self) -> Option<&QWindow> {
        let x = self.qpointer.cpp_ptr();
        if x.is_null() {
            None
        } else {
            Some(&self.qwindow_buffer)
        }
    }

    /// Returns true if the [QWindow] was default constructed or constructed with an object which
    /// is now deleted
    pub fn is_null(&self) -> bool {
        self.qpointer.cpp_ptr().is_null()
    }
}

impl Default for QWindowPointer {
    fn default() -> Self {
        Self {
            qpointer: Default::default(),
            qwindow_buffer: QWindow::null(),
        }
    }
}

impl Clone for QWindowPointer {
    fn clone(&self) -> Self {
        let qpointer = self.qpointer.clone();
        // SAFETY:
        //   - if `qpointer.cpp_ptr` is not `null`, then it points to a valid `QWindow` object (cf
        //     invariant of [QWindowPointer] either valid or null
        //   - in case `qpointer.cpp_ptr == null`, safety follows by definition (cf
        //     [QWindow::from_ptr]'s requirements)
        let qwindow_buffer = unsafe { QWindow::from_ptr(qpointer.cpp_ptr()) };
        Self {
            qpointer,
            qwindow_buffer,
        }
    }
}

impl PropertyType for QWindowPointer {
    fn register_type(_name: &CStr) -> i32 {
        // QWindow*, as a native Qt type, is of course already registered; we just need to look up
        // its id.
        cpp!(unsafe [] -> i32 as "int" {
            return QMetaType::fromType<QWindow*>().id();
        })
    }

    // This function is called during generated QObject's `qt_static_metacall` for the "ReadProperty"
    // index. The `argument_array` is the buffer for arguments and return values that is passed to
    // `qt_static_metacall`. In case of ReadProperty, the result should be stored at the first
    // position of `argument _array`
    //
    // (cf, e.g. https://www.kdab.com/~volker/devdays/2014/QtDevDays2014-DIY-moc.pdf (page 16))
    unsafe fn pass_to_qt(&mut self, argument_array: *mut ffi::c_void) {
        // the first element of the argument array is a buffer for the result.
        let result_buffer = argument_array as *mut *const ffi::c_void;

        // store our pointer in the first element of the result_buffer array
        if self.is_null() {
            *result_buffer = std::ptr::null();
        } else {
            let cpp_ptr = self.qpointer.cpp_ptr();
            *result_buffer = cpp_ptr;
        }
    }

    // This function is called during generated QObject's `qt_static_metacall` for the "WriteProperty"
    // index. The `argument_array` is the buffer for arguments and return values that is passed to
    // `qt_static_metacall`. In case of "WriteProperty", the value to set the property to is at the first
    // position of `argument _array`.
    //
    // (cf, e.g. https://www.kdab.com/~volker/devdays/2014/QtDevDays2014-DIY-moc.pdf (page 16))
    unsafe fn read_from_qt(argument_array: *const ffi::c_void) -> Self {
        // The first element of `argument_array` is the pointer to the QObject that qml wants us to
        // set the property to
        let qwindow_ptr: *const ffi::c_void = *(argument_array as *const *mut ffi::c_void);

        // SAFETY: we registered a "QWindow*" in register_type; thus qml should have given us a
        // "QWindow*"
        QWindowPointer::from_ptr(qwindow_ptr)
    }
}
