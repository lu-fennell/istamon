



# Unsorted collection of TODOs

Should be sorted into proper issues, eventually...

## build pipeline

- [ ] Manually run and create release for tags
- [ ] smaller image for istamon-cli (saves time)

## istamon-cli

- [ ] important methods missing on `icinga::Client`
  - [ ] status
  - [ ] acknowledge (add/remove)
  - [ ] downtime (add/remove)
  - [ ] reschedule checks
- [ ] filter by icinga-user
- [ ] more ways to provide passwords
  - [ ] kwallet
  - [ ] env variable
  - [ ] via terminal
  - [ ] xdg secret service API
- [ ] Provide Rust types for all icinga's json response objects
- [ ] CLI: allow to show status of a single host
- [ ] CLI: show more than one line of check output
- [ ] CLI: print duration for "since" and "next check" istead of just the
  timestamp
- [ ] provide a way to synchronize on new results when requesting a recheck.
- [ ] CLI: improve text output, in particular "up and well"
- [ ] allow to compile with rustls instead of openssl
- [ ] cfg: don't silently ignore a missing cert file in `get_cert_path`
- [ ] cfg: allow urls without credentials or just the username.. also allow to
  specify credentials
  as config fields
- [ ] improve error messages
- [ ] `types::raw` does not completely represent Icinga's responses
- [ ] `types::Service` and `types::Host`'s `handled` field should not be
  public.. probably `handled` should only appear as a field in `types::raw`.
- [ ] CLI: acknowledge needs to specify a user as author.. currently `icingaadmin` is
  hard-coded. Note that this has to be an icinga user, not an icinga API user.
- [ ] CLI/cfg: create initial config (and config dir) if it does not exist
- [ ] CLI: allow to specify tls cert at the command line
- [ ] CLI: show ack'ed hosts/services in the summary
- [ ] CLI: show more info about ack'ed hosts
- [ ] cli: unit tests for command args parsing


## istamon

- [ ] config editor: properly restart clients, when config changed
- [ ] config editor: allow to edit and select multiple configs
- [ ] config editor: handle the case where the config to be edited is faulty.. we should ask if we should override it
- [ ] config workflow: clearer structure (or at least documentation) how configuration is validated and set. Also, remove the "no valid monitor configuration found" error in `IstamonContainer::saveCfg`
- [ ] explicit view-model field for cfg/client name, and display it in a more elegant way 
- [ ] move icinga-mock to clap
- [ ] unify the implementation of the ResultMaps
- [ ] log window, showing errors of "background processes", etc
- [ ] qml: Window should have a max height
- [ ] qml: Rather than disabling "close" have a smart `onClosing` handler that
  knows to minimize when clicked and to clise the system is shutting down
  (there is are hooks in Qt for detecting shutdown)
- [ ] qml: fix the layout FIXMEs
- [ ] qml: figure out why I cannot inline `IstamonListModel` with id `_istamonListModel`.
- [ ] find a better way to start polling than to use the `Component.onCompleted` handler
- [ ] allow to configure the pollling interval 
- [ ] Implement single-instance startup: 
  - [ ] add a `--kill` switch
- [ ] allow connecting without credentials (cf similar issue in `icinga-client`)
- [ ] improve error messages
- [ ] replace `Client::request` with more high-level methods on `Client`, once they are implemented.
- [ ] use `CONST` in `QObject` definitions, where appropriate
- [ ] name the members of `Objects`s in snake case (?)
- [ ] what should the icon be in case of `icon_name == Default::default()`
  (which is the the empty String, presumably)
- [ ] why do I have to maintain an `icon_count` field in `IstamonContainer`
- [ ] use a single signal `state_changed` to notify the fields of `IstamonContainer`
- [ ] Show errors on `start` (and otherwise) in an error screen
- [ ] Don't unwrap the `client` in `start`
- [ ] Why does the app break when I rename `IstamonListItem::itemState` to
  `IstamonListItem::state`?.
- [ ] no app icon on wayland (at least with the flatpak)
- [ ] support multiple cfgs also in the gui configuration
- [ ] report errors of `monitoring_task` in gui
