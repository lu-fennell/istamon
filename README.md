# istamon and icinga-client

Two tools and a library for inspecting the results of [Icinga](https://icinga.com/) monitoring systems.

- [**istamon**](istamon) is a Desktop application that displays the service and
  hosts states of an Icinga instance.
- [**istamon-cli**](istamon) a command-line version of *istamon*
- [**icinga-client**](icinga-client) a Rust client-library for the
  [Icinga-API](https://icinga.com/docs/icinga-2/snapshot/doc/12-icinga2-api/).


