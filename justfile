[working-directory: 'flatpak']
build-flatpak:
  ./flatpak.sh

build-istamon-cli:
  cargo build --release --bin istamon-cli

install-flatpak: build-flatpak
  flatpak install -y --user flatpak/istamon.flatpak  --reinstall

[working-directory: 'build-image']
build-gitlab-container:
  buildah build -t registry.gitlab.com/lu-fennell/istamon
