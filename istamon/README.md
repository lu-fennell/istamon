# istamon

A Desktop application for Linux and a cli that displays the service and host states of an
[Icinga](https://icinga.com/) instance. 

Icinga is an IT monitoring system that does periodic reachability checks for hosts and health checks for services on these hosts. Istamon shows the current status of these checks.

Istamon is inspired by [nagstamon](https://nagstamon.de/). It currently has way less features than nagstamon, but it also does not require [icingaweb2](https://icinga.com/docs/icinga-web-2/latest/doc/01-About/).

## Disclaimer 

Istamon still only has a minimal set of features. The only reasons I can think of to prefer it over
[nagstamon](https://nagstamon.de/) currently would be:

- You want a graphical overview of the monitoring state without running
  [icingaweb2](https://icinga.com/docs/icinga-web-2/latest/doc/01-About/) in
  addition to the icinga core service.
- You'd rather have Qt and Kirigami installed instead of python and GTK

## Build instructions 

### istamon-cli

On an Debian-based distribution, the following packages/dependencies are required for building istamon-cli:

- libssl-dev
- qtbase5-dev 
- qtbase5-dev-tools 
- qtdeclarative5-dev

From the repository root, run:

```
cargo build --release --bin istamon-cli
```

On successful completion of the build, the `istamon-cli` binary is located at `target/release/istamon-cli`.

### istamon (flatpak)

A flatpak build requires a working flatpak installation, the flathub repository enabled, and the `flatpak-builder` tool. On Debian-based distributions, you can get this by runnung 

```
sudo apt-get install flatpak flatpak-builder
flatpak remote-add --user --if-not-exists flathub "https://flathub.org/repo/flathub.flatpakrepo"
```

To build a flatpak for `istamon`, change into the `flatpak` directory and run 

```
bash flatpak.sh
```

The result should be a `istamon.flatpak` file that you can install, e.g., 

```
flatpak install --user istamon.flatpak
```

### istamon (with KDE dependencies installed)

If you are running on a proper KDE installation, chances are you can build `istamon` directly.  

From the repository root, run:

```
cargo build --release --bin istamon
```

On successful completion of the build, the `istamon` binary is located at `target/release/istamon`.

## Running istamon

When installed as a flatpak, just run istamon via the application menu. On first start you will be prompted to configure the monitoring server to use. (See also [Configuration](#configuration))

## Running istamon-cli

`istamon-cli` provides several commands. They are best described by the cli's `--help` text.

```
Simple Desktop application and cli to display the service and host states of an Icinga instance.

Usage: istamon-cli <COMMAND>

Commands:
  rest-api           make a raw request to the icinga api
  status             Show monitoring results
  ack                Acknowledge a host or service
  host-notification  Send a custom notification for a host
  kwallet            edit kwallet passwords for accessing monitors
  help               Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help
  -V, --version  Print version
```

The get help for the command, run `istamon-cli <command> --help`.





## Configuration

The configuration file is located at `$XDG_CONFIG_HOME/istamon/istamon.toml`. It configures the monitors that istamon will query. When running `istamon` (the gui), and no config file is present, it opens a configuration dialog that allows to define and save a configuration.

Here is an example configuration:

```toml
# The sections define "monitor configuration"

# A monitor named "default"
[default]
# mandatory, takes urls of the form "http(s)://[user[:password]@]hostname:port/"
url = "https://icingaweb2@my-icinga-server.com:5665/"
# optional, the api to use, currently, either "icinga" or "icingaweb"
api = "icinga"
# optional, path to a certificate for the icinga api 
#  (typically the certificate can be copied from the icinga host at /var/lib/icinga2/ca/ca.crt)
ca_cert = "/home/fennell/.config/istamon/ca.crt"
# optional, interval in seconds to poll the server for results, defaults to 5
poll_interval = 5

# another monitor, running locally
[test]
url = "http://127.0.0.1:5666/"
poll_interval = 12
```

If the `url` contains a user but no password, `istamon` tries to get the password from `kwallet`. Use the `istamon-cli kwallet` subcommand to set a password for a monitor in kwallet.

The configured monitors can be passed to `istamon` or `istamon-cli` by name on the command line. E.g.:

```
istamon-cli status default test
```

If no monitory is given, the first configured monitor in the configuration file is chosen by default.


