use std::{convert::TryInto, thread};
use thiserror::Error;
use zbus::{dbus_interface, fdo, Interface};

struct IstamonDbusService {
    toggle_callback: Box<dyn Fn(()) -> () + Send + 'static>,
    show_callback: Box<dyn Fn(()) -> () + Send + 'static>,
    hide_callback: Box<dyn Fn(()) -> () + Send + 'static>,
}

#[dbus_interface(name = "de.lu_fennell.istamon.Istamon")]
impl IstamonDbusService {
    fn toggle(&self) -> () {
        (&self.toggle_callback)(())
    }

    fn hide(&self) -> () {
        (&self.hide_callback)(())
    }

    fn show(&self) -> () {
        (&self.show_callback)(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum IstamonDbServiceMethodName {
    Show,
    Toggle,
    Hide,
}

impl IstamonDbServiceMethodName {
    fn name(&self) -> &'static str {
        match self {
            IstamonDbServiceMethodName::Show => "Show",
            IstamonDbServiceMethodName::Toggle => "Toggle",
            IstamonDbServiceMethodName::Hide => "Hide",
        }
    }
}

pub fn service_name() -> &'static str {
    "de.lu_fennell.istamon.Istamon"
}

pub fn object_path() -> &'static str {
    "/de/lu_fennell/istamon/Istamon"
}

pub fn interface_name() -> &'static str {
    <IstamonDbusService as Interface>::name()
}

#[derive(Error, Debug)]
pub enum DbusServiceError {
    #[error("Istamon dbus service not found")]
    ServiceNotFound,
    #[error("Dbus error: {0}")]
    DbusError(#[from] zbus::Error),
}

pub fn invoke_toggle() -> Result<(), DbusServiceError> {
    invoke(IstamonDbServiceMethodName::Toggle)
}

pub fn invoke_show() -> Result<(), DbusServiceError> {
    invoke(IstamonDbServiceMethodName::Show)
}

pub fn invoke_hide() -> Result<(), DbusServiceError> {
    invoke(IstamonDbServiceMethodName::Hide)
}

fn invoke(method_name: IstamonDbServiceMethodName) -> Result<(), DbusServiceError> {
    let connection = zbus::Connection::new_session()?;
    match connection.call_method(
        Some(service_name()),
        object_path(),
        Some(interface_name()),
        method_name.name(),
        &(),
    ) {
        Ok(_) => Ok(()),
        Err(zbus::Error::MethodError(t, _, _))
            if t == "org.freedesktop.DBus.Error.ServiceUnknown" =>
        {
            Err(DbusServiceError::ServiceNotFound)
        }
        Err(e) => Err(e.into()),
    }
}

pub fn start_toggle_handler(
    toggle_callback: impl Fn(()) -> () + Send + 'static,
    show_callback: impl Fn(()) -> () + Send + 'static,
    hide_callback: impl Fn(()) -> () + Send + 'static,
) -> anyhow::Result<()> {
    let connection = zbus::Connection::new_session()?;
    fdo::DBusProxy::new(&connection)?.request_name(
        service_name(),
        fdo::RequestNameFlags::ReplaceExisting.into(),
    )?;

    #[allow(unreachable_code)]
    let instantiate_greeter = move || {
        let mut object_server = zbus::ObjectServer::new(&connection);
        let istamon = IstamonDbusService {
            toggle_callback: Box::new(toggle_callback),
            show_callback: Box::new(show_callback),
            hide_callback: Box::new(hide_callback),
        };
        object_server.at(&object_path().try_into()?, istamon)?;
        loop {
            if let Err(err) = object_server.try_handle_next() {
                eprintln!("{}", err);
            }
        }
        // unreachable.. just to help type inference
        Ok(()) as zbus::Result<()>
    };

    thread::spawn(move || {
        if let Err(e) = instantiate_greeter() {
            eprintln!("Error starting dbus service: {e}");
        }
    });
    Ok(())
}
