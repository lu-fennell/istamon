use std::{
    borrow::Borrow,
    collections::{BTreeMap, HashMap},
    error::Error,
    fmt::Display,
    fs, io,
    iter::FromIterator,
    path::{Path, PathBuf},
    str::FromStr,
    time::Duration,
};

use anyhow::Context;
use directories::ProjectDirs;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use toml_edit::DocumentMut;

use icinga_client::client::{self, Api, Client, Credentials, IcingaUrl};

pub const MONITOR_CONFIG_NAME: &'static str = "istamon.toml";
pub const OLD_MONITOR_CONFIG_NAME: &'static str = "istamon-servers.toml";
pub const PROJECT_NAME: &'static str = "istamon";
pub const QUALIFIER: &'static str = "de.lu-fennell";

pub const DEFAULT_CONFIG_ENTRY_NAME: &'static str = "default";
pub const DEFAULT_POLL_INTERVALL: Duration = Duration::new(5, 0);

pub trait PasswordManager {
    fn load_password(&self, host: &str, user: &str) -> anyhow::Result<Option<String>>;
    fn store_password(&self, host: &str, user: &str, password: &str) -> anyhow::Result<()>;

    fn name(&self) -> String;
}

/// Configuration for a single monitor connection. Typically derived from a config file and/or cli arguments.
/// cf [IcingaApiServer] and [cli] module
#[derive(Debug, Clone, PartialEq)]
pub struct MonitorCfg {
    url: IcingaUrl,
    api: Api,
    credentials: Option<Credentials>,
    ca_cert: Option<PathBuf>,
    // Note: if there is no password, then `password_source == PasswordSource::None`
    password_source: PasswordSource,
    poll_interval: Duration,
}

#[derive(Debug, Error)]
pub enum MonitorCfgError {
    #[error("{0}")]
    NoMonitorFound(String),
    #[error(transparent)]
    OtherError(#[from] anyhow::Error),
}

pub type MonitorCfgs = HashMap<String, MonitorCfg>;
impl MonitorCfg {
    /// Select a [MonitorCfg] from given `entry_name_or_url` by first trying to look up  `entry_name_or_url` in `monitor_cfgs`,
    /// and otherwise trying to derive the [MonitorCfg] from the given url alone. The `password_loader` is used to look up any unspecified passwords.
    pub fn select_from_config_entries(
        password_loader: Option<&dyn PasswordManager>,
        monitor_cfgs: &MonitorCfgs,
        entry_name_or_url: &str,
    ) -> Result<MonitorCfg, MonitorCfgError> {
        let entry_name = entry_name_or_url;
        let selected_entry = monitor_cfgs.get(entry_name);

        let mut cfg = match (selected_entry, entry_name_or_url) {
            (Some(selected_entry), _) => Ok(selected_entry.clone()),
            (None, url_string) => {
                let maybe_url: Result<IcingaUrl, String> = url_string.parse();
                match maybe_url {
                    Ok(url) => {
                        let (url, credentials, password_source) = credentials_from_url(&url);
                        Ok(MonitorCfg {
                            url,
                            api: Api::Icinga,
                            credentials,
                            ca_cert: None,
                            password_source,
                            poll_interval: DEFAULT_POLL_INTERVALL,
                        })
                    }
                    Err(e) => Err(MonitorCfgError::NoMonitorFound(format!(
                        "'{}' is not the name of a predefined configuration and it also cannot be parsed as a url: {}",
                        url_string, e
                    ))),
                }
            }
        }?;
        // if we did not get any password, look it up with the loader
        if let Some((user, None)) = cfg.credentials {
            if let Some(password_loader) = password_loader {
                let password = password_loader.load_password(&cfg.url.host_str(), &user)?;
                if password.is_some() {
                    cfg.password_source = PasswordSource::Encrypted {
                        provider: password_loader.name(),
                    }
                }
                cfg.credentials = Some((user, password));
            } else {
                cfg.credentials = Some((user, None))
            }
        }
        Ok(cfg)
    }

    pub fn new_with_credentials(
        url: IcingaUrl,
        user: String,
        password: String,
        password_source: PasswordSource,
        ca_cert: Option<PathBuf>,
    ) -> Self {
        Self {
            url,
            api: Api::Icinga,
            credentials: Some((user, Some(password))),
            ca_cert,
            password_source,
            poll_interval: DEFAULT_POLL_INTERVALL,
        }
    }

    pub fn new_without_credentials(url: IcingaUrl, ca_cert: Option<PathBuf>) -> Self {
        Self {
            url,
            api: Api::Icinga,
            credentials: None,
            ca_cert,
            password_source: PasswordSource::None,
            poll_interval: DEFAULT_POLL_INTERVALL,
        }
    }

    pub fn into_client(self) -> client::Result<Client> {
        Client::new(self.url, self.api, self.ca_cert, self.credentials)
    }

    /// Get a reference to the cfg's credentials.
    pub fn credentials(&self) -> Option<&Credentials> {
        self.credentials.as_ref()
    }

    /// Get a reference to the cfg's url.
    pub fn url(&self) -> &IcingaUrl {
        &self.url
    }

    /// Get a reference to the cfg's password source.
    pub fn password_source(&self) -> &PasswordSource {
        &self.password_source
    }

    /// Get a reference to the cfg's ca cert.
    pub fn ca_cert(&self) -> Option<&PathBuf> {
        self.ca_cert.as_ref()
    }

    pub fn poll_interval(&self) -> Duration {
        self.poll_interval
    }
}

impl<M: Borrow<MonitorCfgFileEntry>> From<M> for MonitorCfg {
    fn from(value: M) -> Self {
        let value = value.borrow();
        let (url, credentials, password_source) = credentials_from_url(&value.url);
        Self {
            url,
            api: value.api,
            credentials,
            ca_cert: value.ca_cert.as_ref().map(PathBuf::from),
            password_source,
            poll_interval: Duration::new(value.poll_interval.into(), 0),
        }
    }
}

fn credentials_from_url(url: &IcingaUrl) -> (IcingaUrl, Option<Credentials>, PasswordSource) {
    let (credentials, url) = url.split_credentials();
    let password_source = credentials
        .as_ref()
        .and_then(|(_, p)| p.as_ref().map(|_| PasswordSource::Unencrypted))
        .unwrap_or(PasswordSource::None);
    (url, credentials, password_source)
}

#[derive(Debug, Clone, PartialEq)]
pub enum PasswordSource {
    None,
    Encrypted { provider: String },
    Unencrypted,
}

/// A map from names to [MonitorCfg]s that possibly have failed to be read.
///
/// a [MonitorCfgMap] is never empty, cf [Self::from_urls]
#[derive(Debug)]
pub struct MonitorCfgMap {
    map: BTreeMap<String, Result<MonitorCfg, MonitorCfgError>>,
}

impl MonitorCfgMap {
    pub fn from_single_cfg(name: String, cfg: MonitorCfg) -> Self {
        let map = BTreeMap::from_iter([(name, Ok(cfg))]);
        Self { map }
    }

    /// Get a [MonitorCfgMap] from `url_or_entry_names`, collected by either
    /// (1) looking up an entry name in `monitor_cfgs`, or
    /// (2) deriving the [MonitorCfg] from the url directly.
    ///
    /// If `url_or_entry_names` is empty, `default_monitor` is used as the single entry.
    pub fn from_urls<S: AsRef<str>>(
        loader: Option<&dyn PasswordManager>,
        monitor_cfgs: &MonitorCfgs,
        url_or_entry_names: &[S],
        default_monitor: &str,
    ) -> Self {
        let mk_cfg = |u: &str| -> Result<MonitorCfg, MonitorCfgError> {
            Ok(MonitorCfg::select_from_config_entries(
                loader,
                monitor_cfgs,
                u,
            )?)
        };
        let map = if url_or_entry_names.is_empty() {
            BTreeMap::from_iter([("".into(), mk_cfg(default_monitor))])
        } else {
            url_or_entry_names
                .into_iter()
                .map(|u| (u.as_ref().to_string(), mk_cfg(u.as_ref())))
                .collect()
        };
        Self { map }
    }

    pub fn into_iter(self) -> impl Iterator<Item = (String, Result<MonitorCfg, MonitorCfgError>)> {
        self.map.into_iter()
    }
    pub fn into_iter_ok(self) -> impl Iterator<Item = (String, MonitorCfg)> {
        self.map.into_iter().filter_map(|(n, r)| match r {
            Ok(cfg) => Some((n, cfg)),
            Err(_) => None,
        })
    }

    pub fn iter(&self) -> impl Iterator<Item = (&String, &Result<MonitorCfg, MonitorCfgError>)> {
        self.map.iter()
    }

    pub fn iter_ok(&self) -> impl Iterator<Item = (&String, &MonitorCfg)> {
        self.map.iter().filter_map(|(n, r)| match r {
            Ok(cfg) => Some((n, cfg)),
            Err(_) => None,
        })
    }

    pub fn iter_err(&self) -> impl Iterator<Item = (&String, &MonitorCfgError)> {
        self.map.iter().filter_map(|(n, r)| match r {
            Ok(_) => None,
            Err(e) => Some((n, e)),
        })
    }

    pub fn first_valid_cfg_mut(&mut self) -> Option<&mut MonitorCfg> {
        self.map.values_mut().filter_map(|r| r.as_mut().ok()).next()
    }

    pub fn insert(
        &mut self,
        name: String,
        cfg: MonitorCfg,
    ) -> Option<Result<MonitorCfg, MonitorCfgError>> {
        self.map.insert(name, Ok(cfg))
    }
}

/// Configuration for connecting to a monitor.
///
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
struct MonitorCfgFileEntry {
    pub url: IcingaUrl,
    #[serde(default)]
    pub api: Api,
    pub ca_cert: Option<String>,
    #[serde(default = "MonitorCfgFileEntry::default_poll_interval")]
    pub poll_interval: u32,
}

impl MonitorCfgFileEntry {
    fn default_poll_interval() -> u32 {
        5
    }
}

impl From<&MonitorCfg> for MonitorCfgFileEntry {
    fn from(cfg: &MonitorCfg) -> Self {
        let mut url = cfg.url.clone();
        if let Some((user, password)) = &cfg.credentials {
            url.set_username(user);
            if cfg.password_source == PasswordSource::Unencrypted {
                url.set_password(password.as_ref().map(|s| s as &str));
            }
        }
        Self {
            url,
            api: cfg.api.clone(),
            ca_cert: cfg
                .ca_cert
                .as_ref()
                .map(|p| p.to_string_lossy().to_string()),
            poll_interval: cfg.poll_interval.as_secs() as u32,
        }
    }
}

#[derive(Debug)]
pub enum LoadError {
    Toml(PathBuf, toml::de::Error),
    Io(PathBuf, io::Error),
}

impl Display for LoadError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LoadError::Toml(p, e) => {
                write!(f, "error deserializing {}: {}", p.to_string_lossy(), e)
            }
            LoadError::Io(p, e) => write!(f, "error reading {}: {}", p.to_string_lossy(), e),
        }
    }
}

impl Error for LoadError {}

/// If the `cfg_path` exists, add the given cfg under `name` in the toml config file given by `cfg_path`.
/// Otherwise, create a `cfg_path` and its parent directories and save and initial `cfg` under `name`.
pub fn save_cfg_in_toml_file(cfg_path: &Path, name: &str, cfg: &MonitorCfg) -> anyhow::Result<()> {
    if !cfg_path.exists() {
        if let Some(parent_dir) = cfg_path.parent() {
            fs::create_dir_all(parent_dir)?;
        }
    }

    update_monitor_config_file(cfg_path, name, &cfg)?;
    Ok(())
}

fn update_monitor_config_file(
    config_file_path: &Path,
    name: &str,
    entry: &MonitorCfg,
) -> anyhow::Result<()> {
    let empty_when_not_found = |e: io::Error| {
        if let io::ErrorKind::NotFound = e.kind() {
            Ok("".into())
        } else {
            Err(e)
        }
    };
    let old_content = fs::read_to_string(config_file_path)
        .or_else(empty_when_not_found)
        .context(format!(
            "reading old content of `{}'",
            config_file_path.display()
        ))?;
    fs::write(
        &config_file_path,
        update_toml(&old_content, name, &entry.into())?,
    )
    .context(format!(
        "writing monitor config file to `{}'",
        config_file_path.display(),
    ))?;
    Ok(())
}

fn update_toml(
    old_content: &str,
    name: &str,
    entry: &MonitorCfgFileEntry,
) -> anyhow::Result<Vec<u8>> {
    let mut old_doc = DocumentMut::from_str(old_content).context(format!("invalid config file"))?;
    let table = old_doc.as_table_mut();
    table.insert(name, toml_edit::ser::to_document(entry)?.as_item().clone());
    Ok(old_doc.to_string().into_bytes())
}

pub fn load_monitor_config_entries_or_empty<T: AsRef<Path>>(
    config_file_path: Option<T>,
) -> Result<MonitorCfgs, LoadError> {
    match config_file_path {
        Some(p) if p.as_ref().exists() => load_monitor_config_entries(p.as_ref()),
        _ => Ok(HashMap::new()),
    }
}

pub fn load_monitor_config_entries(config_file_path: &Path) -> Result<MonitorCfgs, LoadError> {
    let content = fs::read_to_string(&config_file_path)
        .map_err(|e| LoadError::Io(config_file_path.to_path_buf(), e))?;
    let entry: HashMap<String, MonitorCfgFileEntry> =
        toml::from_str(&content).map_err(|e| LoadError::Toml(config_file_path.to_path_buf(), e))?;
    Ok(entry
        .into_iter()
        .map(|(n, e)| (n, MonitorCfg::from(e)))
        .collect())
}

pub fn default_config_file() -> Option<PathBuf> {
    default_config_dir().map(|p| {
        let mut cfg_file = p.join(MONITOR_CONFIG_NAME);
        let old_cfg_file = p.join(OLD_MONITOR_CONFIG_NAME);
        if !cfg_file.exists() && old_cfg_file.exists() {
            log::warn!(
                "Using deprecated config file {}. Please rename it to {}.",
                old_cfg_file.display(),
                cfg_file.display()
            );
            cfg_file = old_cfg_file;
        }
        cfg_file
    })
}

fn default_config_dir() -> Option<PathBuf> {
    ProjectDirs::from(QUALIFIER, "", PROJECT_NAME)
        .map(|project_dirs| project_dirs.config_dir().to_path_buf())
}

#[cfg(test)]
mod test {

    use super::*;
    use bon::builder;
    use indoc::indoc;
    use pretty_assertions::assert_eq;

    fn no_entries() -> MonitorCfgs {
        HashMap::new()
    }

    #[builder]
    fn cfg_from_configured_monitors(
        password_manager: Option<&dyn PasswordManager>,
        config_entries: Option<MonitorCfgs>,
        url: Option<&str>,
    ) -> Result<MonitorCfg, MonitorCfgError> {
        MonitorCfg::select_from_config_entries(
            password_manager,
            &config_entries.unwrap_or(no_entries()),
            url.unwrap_or(&DEFAULT_CONFIG_ENTRY_NAME),
        )
    }

    #[builder]
    fn cfg(
        url: &str,
        api: Option<Api>,
        credentials: Option<(String, Option<String>)>,
        password_source: Option<PasswordSource>,
        ca_cert: Option<PathBuf>,
        poll_interval: Option<Duration>,
    ) -> MonitorCfg {
        MonitorCfg {
            url: url.parse().unwrap(),
            api: api.unwrap_or_default(),
            credentials,
            ca_cert,
            password_source: password_source.unwrap_or(PasswordSource::None),
            poll_interval: poll_interval.unwrap_or(DEFAULT_POLL_INTERVALL),
        }
    }

    #[test]
    fn test_cfg_only_cli_url_no_auth() {
        let cfg_result = cfg_from_configured_monitors()
            .url("http://127.0.0.1:8080")
            .call();
        assert_eq!(
            cfg_result.unwrap(),
            cfg()
                .url("http://127.0.0.1:8080")
                .password_source(PasswordSource::None)
                .call()
        )
    }

    #[test]
    fn test_cfg_only_cli_url_auth() {
        let cfg_result = cfg_from_configured_monitors()
            .url("http://test_user:test_pass@127.0.0.1:8080")
            .call();
        assert_eq!(
            cfg_result.unwrap(),
            cfg()
                .url("http://127.0.0.1:8080")
                .credentials(("test_user".to_string(), Some("test_pass".to_string())))
                .password_source(PasswordSource::Unencrypted)
                .call()
        )
    }

    #[test]
    fn test_cfg_only_cli_no_url() {
        let cfg_result = cfg_from_configured_monitors().call();
        assert_eq!(
            cfg_result.unwrap_err().to_string(),
            "'default' is not the name of a predefined configuration and it also cannot be parsed as a url: Url 'default' is invalid: relative URL without a base"
                .to_string()
        )
    }

    fn default_entry(url: &str) -> MonitorCfgs {
        HashMap::from([(
            "default".to_string(),
            MonitorCfg::from(&MonitorCfgFileEntry {
                url: url.parse().unwrap(),
                api: Default::default(),
                ca_cert: None,
                poll_interval: 17,
            }),
        )])
    }

    #[test]
    fn test_cfg_default_configured_no_auth() {
        let cfg_result = cfg_from_configured_monitors()
            .config_entries(default_entry("https://example.com"))
            .call();
        assert_eq!(
            cfg_result.unwrap(),
            cfg()
                .url("https://example.com")
                .poll_interval(Duration::new(17, 0))
                .call()
        )
    }

    #[test]
    fn test_cfg_default_configured_auth() {
        let cfg_result = cfg_from_configured_monitors()
            .config_entries(default_entry("https://test:mutti123@example.com"))
            .url("default")
            .call();

        assert_eq!(
            cfg_result.unwrap(),
            cfg()
                .url("https://example.com")
                .credentials(("test".to_string(), Some("mutti123".to_string())))
                .password_source(PasswordSource::Unencrypted)
                .poll_interval(Duration::new(17, 0))
                .call()
        )
    }

    #[test]
    fn test_cfg_default_not_configured_no_url() {
        let cfg_result = cfg_from_configured_monitors().call();
        assert_eq!(
            cfg_result.unwrap_err().to_string(),
            "'default' is not the name of a predefined configuration and it also cannot be parsed as a url: Url 'default' is invalid: relative URL without a base".to_string()
        )
    }

    struct TestLoader(&'static str, &'static str);
    impl PasswordManager for TestLoader {
        fn load_password(&self, host: &str, user: &str) -> anyhow::Result<Option<String>> {
            if host == self.0 && user == self.1 {
                Ok(Some("mutti123".to_string()))
            } else {
                Ok(None)
            }
        }

        fn name(&self) -> String {
            "test-loader".to_string()
        }

        fn store_password(&self, _host: &str, _user: &str, _password: &str) -> anyhow::Result<()> {
            unimplemented!()
        }
    }

    #[test]
    fn test_cfg_auth_password_loader() {
        let cfg_result = cfg_from_configured_monitors()
            .password_manager(&TestLoader("example.com", "user"))
            .url("https://user@example.com")
            .call();
        assert_eq!(
            cfg_result.unwrap(),
            cfg()
                .url("https://example.com")
                .credentials(("user".to_string(), Some("mutti123".to_string())))
                .password_source(PasswordSource::Encrypted {
                    provider: "test-loader".to_string()
                })
                .call()
        )
    }

    #[test]
    fn test_cfg_auth_password_loader_user_not_found() {
        let cfg_result = cfg_from_configured_monitors()
            .password_manager(&TestLoader("example.com", "user"))
            .url("https://user2@example.com")
            .call();
        assert_eq!(
            cfg_result.unwrap(),
            cfg()
                .url("https://example.com")
                .credentials(("user2".to_string(), None))
                .call()
        )
    }

    #[test]
    fn test_cfg_auth_password_loader_no_password_for_host() {
        let cfg_result = cfg_from_configured_monitors()
            .password_manager(&TestLoader("example.com", "user"))
            .url("https://user@example2.com")
            .call();
        assert_eq!(
            cfg_result.unwrap(),
            cfg()
                .url("https://example2.com")
                .credentials(("user".to_string(), None))
                .call()
        )
    }

    #[builder]
    fn cfg_entry(
        url: &str,
        api: Option<Api>,
        ca_cert: Option<&str>,
        poll_interval: Option<u32>,
    ) -> MonitorCfgFileEntry {
        MonitorCfgFileEntry {
            url: url.parse().unwrap(),
            api: api.unwrap_or_default(),
            ca_cert: ca_cert.map(str::to_string),
            poll_interval: poll_interval.unwrap_or(DEFAULT_POLL_INTERVALL.as_secs() as u32),
        }
    }

    #[test]
    fn test_cfg2entry_only_url() {
        let cfg = cfg()
            .url("http://127.0.0.1:5665")
            .poll_interval(Duration::new(27, 0))
            .call();
        let entry: MonitorCfgFileEntry = MonitorCfgFileEntry::from(&cfg);
        assert_eq!(
            entry,
            cfg_entry()
                .url("http://127.0.0.1:5665")
                .poll_interval(27)
                .call()
        )
    }

    #[test]
    fn test_cfg2entry_url_with_username_no_password() {
        let cfg = cfg()
            .url("http://127.0.0.1:5665")
            .credentials(("user".to_string(), None))
            .poll_interval(Duration::new(27, 0))
            .call();
        let entry: MonitorCfgFileEntry = MonitorCfgFileEntry::from(&cfg);
        assert_eq!(
            entry,
            cfg_entry()
                .url("http://user@127.0.0.1:5665")
                .poll_interval(27)
                .call()
        )
    }

    #[test]
    fn test_cfg2entry_url_with_username_and_password() {
        let cfg = cfg()
            .url("http://127.0.0.1:5665")
            .credentials(("user".to_string(), Some("mutti123".to_string())))
            .poll_interval(Duration::new(27, 0))
            .password_source(PasswordSource::Unencrypted)
            .call();
        let entry: MonitorCfgFileEntry = MonitorCfgFileEntry::from(&cfg);
        assert_eq!(
            entry,
            cfg_entry()
                .url("http://user:mutti123@127.0.0.1:5665")
                .poll_interval(27)
                .call()
        )
    }

    #[test]
    fn test_cfg2entry_url_with_username_and_password_encrypted() {
        let cfg = cfg()
            .url("http://127.0.0.1:5665")
            .credentials(("user".to_string(), Some("mutti123".to_string())))
            .poll_interval(Duration::new(27, 0))
            .password_source(PasswordSource::Encrypted {
                provider: "provider".to_string(),
            })
            .call();
        let entry: MonitorCfgFileEntry = MonitorCfgFileEntry::from(&cfg);
        assert_eq!(
            entry,
            cfg_entry()
                .url("http://user@127.0.0.1:5665")
                .poll_interval(27)
                .call()
        )
    }

    #[test]
    fn test_cfg2entry_url_with_username_and_password_dontsave() {
        let cfg = cfg()
            .url("http://127.0.0.1:5665")
            .credentials(("user".to_string(), Some("mutti123".to_string())))
            .poll_interval(Duration::new(27, 0))
            .password_source(PasswordSource::None)
            .call();
        let entry: MonitorCfgFileEntry = MonitorCfgFileEntry::from(&cfg);
        assert_eq!(
            entry,
            cfg_entry()
                .url("http://user@127.0.0.1:5665")
                .poll_interval(27)
                .call()
        )
    }

    #[test]
    fn test_cfg2entry_url_with_cacert() {
        let cfg = cfg()
            .url("http://127.0.0.1:5665")
            .credentials(("user".to_string(), Some("mutti123".to_string())))
            .poll_interval(Duration::new(27, 0))
            .password_source(PasswordSource::None)
            .ca_cert(PathBuf::from("/path/to/cert.crt"))
            .call();
        let entry: MonitorCfgFileEntry = MonitorCfgFileEntry::from(&cfg);
        assert_eq!(
            entry,
            cfg_entry()
                .url("http://user@127.0.0.1:5665")
                .poll_interval(27)
                .ca_cert("/path/to/cert.crt")
                .call()
        )
    }

    #[test]
    fn test_cfg2entry_icingaweb() {
        let cfg = cfg()
            .url("http://127.0.0.1:5665")
            .api(Api::IcingaWeb)
            .credentials(("user".to_string(), Some("mutti123".to_string())))
            .poll_interval(Duration::new(27, 0))
            .password_source(PasswordSource::None)
            .ca_cert(PathBuf::from("/path/to/cert.crt"))
            .call();
        let entry: MonitorCfgFileEntry = MonitorCfgFileEntry::from(&cfg);
        assert_eq!(
            entry,
            cfg_entry()
                .url("http://user@127.0.0.1:5665")
                .poll_interval(27)
                .ca_cert("/path/to/cert.crt")
                .api(Api::IcingaWeb)
                .call()
        )
    }

    #[test]
    fn test_edit_default() -> anyhow::Result<()> {
        let cfg_entry = MonitorCfgFileEntry {
            url: "http://user@127.0.0.1:5665".parse().unwrap(),
            api: Api::IcingaWeb,
            ca_cert: Some("/path/to/cert.crt".to_string()),
            poll_interval: 27,
        };
        let table = toml_edit::ser::to_document(&cfg_entry)?;

        assert_eq!(
            table.to_string(),
            indoc! {r#"
               url = "http://user@127.0.0.1:5665/"
               api = "icingaweb"
               ca_cert = "/path/to/cert.crt"
               poll_interval = 27
            "#}
        );
        Ok(())
    }

    #[test]
    fn test_update_monitor_config() -> anyhow::Result<()> {
        let cfg = indoc! {r#"
            [server2]
            url = "http://user@127.0.0.1:5665/"
            api = "icingaweb"
            ca_cert = "/path/to/cert.crt"
            poll_interval = 27

            # This should stay the same
            [server1]
            url = "http://user@127.0.0.2:5665/"
            api = "icingaweb"
            ca_cert = "/path/to/cert.crt"
            poll_interval = 27
        "#};
        let new_entry = MonitorCfgFileEntry {
            url: "http://user@new_server:5665".parse().unwrap(),
            api: Api::IcingaWeb,
            ca_cert: Some("/path/to/cert.crt".to_string()),
            poll_interval: 27,
        };
        let new_cfg_bytes = update_toml(cfg, "server2", &new_entry)?;
        let new_cfg = String::from_utf8_lossy(&new_cfg_bytes);
        assert_eq!(
            new_cfg,
            indoc! {r#"
              [server2]
              url = "http://user@new_server:5665/"
              api = "icingaweb"
              ca_cert = "/path/to/cert.crt"
              poll_interval = 27

              # This should stay the same
              [server1]
              url = "http://user@127.0.0.2:5665/"
              api = "icingaweb"
              ca_cert = "/path/to/cert.crt"
              poll_interval = 27
        "#}
        );
        Ok(())
    }
}
