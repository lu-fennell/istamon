use std::collections::BTreeMap;

use icinga_client::types::{Host, Service};
use smol::{
    channel::{Receiver, Sender},
    stream::{Stream, StreamExt},
    Executor, Task, Timer,
};

use crate::monitor_client::MonitorClient;

type RequestResult = Result<(Vec<Service>, Vec<Host>), String>;
pub type RequestResultMap = BTreeMap<String, RequestResult>;

pub fn monitoring_tasks(
    e: &Executor,
    clients: Vec<MonitorClient>,
) -> impl Stream<Item = RequestResultMap> {
    MonitoringTasks::from_clients(e, clients).stream()
}

struct MonitoringTasks {
    tasks: Vec<Task<()>>,
    results: Receiver<(String, RequestResult)>,
}

impl MonitoringTasks {
    fn from_clients(e: &Executor, clients: Vec<MonitorClient>) -> Self {
        let mut tasks = Vec::new();
        let (s, r) = smol::channel::bounded(1);
        e.spawn_many(
            clients.into_iter().map(|client| {
                let s = s.clone();
                async move { single_client_producer(client, s).await }
            }),
            &mut tasks,
        );
        Self { tasks, results: r }
    }

    fn stream(self) -> impl Stream<Item = RequestResultMap> {
        let mut results: RequestResultMap = Default::default();
        self.results.scan(self.tasks, move |_, (n, v)| {
            results.insert(n.to_string(), v.map_err(|e| e.to_string()));
            let cloned = results.clone();
            Some(cloned)
        })
    }
}

async fn single_client_producer(client: MonitorClient, s: Sender<(String, RequestResult)>) {
    loop {
        match s.send(request(&client)).await {
            Ok(()) => Timer::after(client.poll_interval()).await,
            Err(e) => {
                log::error!("Client result receiver was closed: {e}");
                return;
            }
        };
    }
}

fn request(client: &MonitorClient) -> (String, RequestResult) {
    let services_result = client.get_services();
    let hosts_result = client.get_hosts();
    log::debug!("Received results from {}", client.name());
    (
        client.name().to_string(),
        services_result
            .and_then(|new_items| Ok((new_items, hosts_result?)))
            .map_err(|e| e.to_string()),
    )
}
