use std::convert::TryFrom;

use clap::Parser;

#[derive(Debug, PartialEq)]
pub enum Method {
    Get,
    Post,
}

impl TryFrom<&str> for Method {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "GET" => Ok(Method::Get),
            "POST" => Ok(Method::Post),
            _ => Err(format!("Method not supported: {}", value)),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum AckTarget {
    Host(String),
    Service(String),
}

impl AckTarget {
    pub fn query(&self) -> Vec<(&'static str, String)> {
        match self {
            AckTarget::Host(host_name) => {
                vec![("host", host_name.to_string())]
            }
            AckTarget::Service(service_name) => {
                vec![("service", service_name.to_string())]
            }
        }
    }

    pub fn object_type(&self) -> String {
        match self {
            AckTarget::Host(_) => "Host".to_string(),
            AckTarget::Service(_) => "Service".to_string(),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ClientCommand {
    RestApi {
        method: Method,
        read_body: bool,
        path: String,
    },
    Ack {
        target: AckTarget,
        until: Option<u64>,
        comment: Option<String>,
    },
    HostNotification {
        host: String,
        comment: String,
    },
}

#[derive(Debug, PartialEq)]
pub enum KWalletCommand {
    WritePassword,
    ReadPassword,
    RemovePassword,
}

#[derive(Debug, PartialEq)]
pub enum Command {
    Client {
        url_string: Option<String>,
        command: ClientCommand,
    },
    Status {
        url_strings: Vec<String>,
        verbose: bool,
    },
    KWallet {
        url_string: Option<String>,
        command: KWalletCommand,
    },
}

pub fn parse_args() -> Command {
    parse_model::CliOptions::parse().into()
}

mod parse_model {
    use clap::{Args, Parser, ValueEnum};

    use super::*;

    #[derive(Debug, Parser)]
    #[clap(author, about, version)]
    pub struct CliOptions {
        #[clap(subcommand)]
        command: CliCommand,
    }

    impl Into<Command> for CliOptions {
        fn into(self) -> Command {
            self.command.into()
        }
    }

    #[derive(Debug, Parser)]
    enum CliCommand {
        #[clap(about = "make a raw request to the icinga api")]
        RestApi {
            #[clap(
                long,
                help = "read the request body from stdin",
                default_value_t = false
            )]
            read_body: bool,
            #[clap(short = 'X', default_value_t = MethodArg::Get, value_enum)]
            method: MethodArg,
            #[clap(flatten)]
            monitor: Monitor,
            path: String,
        },
        #[clap(about = "Show monitoring results")]
        Status {
            #[clap(
                long,
                short,
                default_value_t = false,
                help = "Also show handled and ok hosts/services"
            )]
            verbose: bool,
            #[clap(
                help = "urls or configured monitor names to query (default: first configured monitor)"
            )]
            monitor_urls: Vec<String>,
        },
        #[clap(about = "Acknowledge a host or service")]
        Ack {
            #[command(flatten)]
            host_or_service: HostOrService,
            #[clap(long, value_parser = parse_timestamp)]
            until: Option<u64>,
            #[clap(long)]
            comment: Option<String>,
            #[clap(flatten)]
            monitor: Monitor,
        },

        #[clap(about = "Send a custom notification for a host")]
        HostNotification {
            #[clap(long)]
            host: String,
            #[clap(long)]
            comment: String,
            #[clap(flatten)]
            monitor: Monitor,
        },

        #[clap(about = "edit kwallet passwords for accessing monitors")]
        Kwallet {
            #[clap(subcommand)]
            command: CliKwalletCommand,
        },
    }

    #[derive(Debug, Clone, Args)]
    struct Monitor {
        #[clap(
            long,
            help = "url or config name of the monitory to query (default: the first configured monitor)"
        )]
        monitor_url: Option<String>,
    }

    #[derive(Debug, Clone, Parser)]
    enum CliKwalletCommand {
        ReadPassword { monitor_url: Option<String> },
        WritePassword { monitor_url: Option<String> },
        RemovePassword { monitor_url: Option<String> },
    }

    impl Into<Command> for CliKwalletCommand {
        fn into(self) -> Command {
            match self {
                CliKwalletCommand::ReadPassword { monitor_url } => Command::KWallet {
                    url_string: monitor_url,
                    command: KWalletCommand::ReadPassword,
                },
                CliKwalletCommand::WritePassword { monitor_url } => Command::KWallet {
                    url_string: monitor_url,
                    command: KWalletCommand::WritePassword,
                },
                CliKwalletCommand::RemovePassword { monitor_url } => Command::KWallet {
                    url_string: monitor_url,
                    command: KWalletCommand::RemovePassword,
                },
            }
        }
    }

    fn parse_timestamp(s: &str) -> Result<u64, String> {
        s.parse::<u64>()
            .map_err(|_| "could not parse unix-second timestamp".to_owned())
    }

    #[derive(Debug, Clone, Args)]
    #[group(required = true, multiple = false)]
    struct HostOrService {
        #[clap(long)]
        host: Option<String>,
        #[clap(long)]
        service: Option<String>,
    }

    impl Into<AckTarget> for HostOrService {
        fn into(self) -> AckTarget {
            if let Some(host) = self.host {
                AckTarget::Host(host)
            } else if let Some(service) = self.service {
                AckTarget::Service(service)
            } else {
                unreachable!(
                    "clap should have ensured that one of --host or --service was provided"
                )
            }
        }
    }

    impl Into<Command> for CliCommand {
        fn into(self) -> Command {
            match self {
                CliCommand::RestApi {
                    read_body,
                    method,
                    monitor,
                    path,
                } => Command::Client {
                    url_string: monitor.monitor_url,
                    command: ClientCommand::RestApi {
                        method: method.into(),
                        read_body,
                        path,
                    },
                },
                CliCommand::Status {
                    verbose,
                    monitor_urls,
                } => Command::Status {
                    url_strings: monitor_urls,
                    verbose,
                },
                CliCommand::Ack {
                    host_or_service,
                    until,
                    comment,
                    monitor,
                } => Command::Client {
                    url_string: monitor.monitor_url,
                    command: ClientCommand::Ack {
                        target: host_or_service.into(),
                        until,
                        comment,
                    },
                },
                CliCommand::HostNotification {
                    host,
                    comment,
                    monitor,
                } => Command::Client {
                    url_string: monitor.monitor_url,
                    command: ClientCommand::HostNotification { host, comment },
                },
                CliCommand::Kwallet { command } => command.into(),
            }
        }
    }

    #[derive(Debug, Clone, Copy, PartialEq, ValueEnum)]
    #[clap(rename_all = "UPPER")]
    enum MethodArg {
        Get,
        Post,
    }

    impl Into<Method> for MethodArg {
        fn into(self) -> Method {
            match self {
                MethodArg::Get => Method::Get,
                MethodArg::Post => Method::Post,
            }
        }
    }
}

#[cfg(test)]
mod test {
    use std::{ffi::OsString, iter};

    use super::*;
    use assertables::*;
    use clap::Parser;

    fn get_options<I: IntoIterator<Item = T>, T: Into<OsString> + Clone>(
        argv: I,
    ) -> Result<Command, String> {
        let argv: Vec<OsString> = iter::once("icinga-client".into())
            .chain(argv.into_iter().map(|i| i.into()))
            .collect();
        parse_model::CliOptions::try_parse_from(argv)
            .map_err(|e| e.to_string())
            .map(|o| o.into())
    }

    fn default_client_command(command: ClientCommand) -> Command {
        Command::Client {
            url_string: None,
            command,
        }
    }

    #[test]
    fn test_rest_api_body() {
        let options = get_options(&["rest-api", "/some/path"]).unwrap();
        assert_eq!(
            options,
            Command::Client {
                url_string: None,
                command: ClientCommand::RestApi {
                    method: Method::Get,
                    read_body: false,
                    path: "/some/path".into()
                }
            }
        );
        let options = get_options(&["rest-api", "--read-body", "/some/path"]).unwrap();
        assert_eq!(
            options,
            default_client_command(ClientCommand::RestApi {
                method: Method::Get,
                read_body: true,
                path: "/some/path".into()
            })
        );
    }

    #[test]
    fn test_rest_api_monitor() {
        let options =
            get_options(&["rest-api", "/some/path", "--monitor-url", "testmonitor"]).unwrap();
        assert_eq!(
            options,
            Command::Client {
                url_string: Some("testmonitor".into()),
                command: ClientCommand::RestApi {
                    method: Method::Get,
                    read_body: false,
                    path: "/some/path".into()
                }
            }
        );
    }

    #[test]
    fn test_rest_api_method() {
        let options = get_options(&["rest-api", "/some/path"]).unwrap();
        assert_eq!(
            options,
            default_client_command(ClientCommand::RestApi {
                method: Method::Get,
                read_body: false,
                path: "/some/path".into()
            })
        );
        let options = get_options(&["rest-api", "-X", "POST", "/some/path"]).unwrap();
        assert_eq!(
            options,
            default_client_command(ClientCommand::RestApi {
                method: Method::Post,
                read_body: false,
                path: "/some/path".into()
            })
        );
    }

    #[test]
    fn test_rest_api_method_invalid() {
        let options: Result<_, String> = get_options(&["rest-api", "-X", "post", "/some/path"]);
        assert_contains!(
            format!("{}", options.err().unwrap()),
            "possible values: GET, POST"
        );
    }

    #[test]
    fn test_status_verbose() {
        let options = get_options(&["status"]).unwrap();
        assert_eq!(
            options,
            Command::Status {
                url_strings: Vec::new(),
                verbose: false
            }
        );
        let options = get_options(vec!["status", "--verbose"]).unwrap();
        assert_eq!(
            options,
            Command::Status {
                url_strings: Vec::new(),
                verbose: true
            }
        );
    }

    #[test]
    fn test_status_monitors() {
        let options = get_options(&["status"]).unwrap();
        assert_eq!(
            options,
            Command::Status {
                url_strings: Vec::new(),
                verbose: false
            }
        );
        let options = get_options(&["status", "testmonitor"]).unwrap();
        assert_eq!(
            options,
            Command::Status {
                url_strings: vec!["testmonitor".into()],
                verbose: false
            }
        );
        let options = get_options(&["status", "testmonitor1", "testmonitor2"]).unwrap();
        assert_eq!(
            options,
            Command::Status {
                url_strings: vec!["testmonitor1".into(), "testmonitor2".into()],
                verbose: false
            }
        );
    }

    #[test]
    fn test_ack_target() {
        let options = get_options(&["ack", "--service", "testservice"]).unwrap();
        assert_eq!(
            options,
            default_client_command(ClientCommand::Ack {
                target: AckTarget::Service("testservice".into()),
                until: None,
                comment: None
            })
        );
        let options = get_options(&["ack", "--host", "testhost"]).unwrap();
        assert_eq!(
            options,
            default_client_command(ClientCommand::Ack {
                target: AckTarget::Host("testhost".into()),
                until: None,
                comment: None
            })
        );
    }

    #[test]
    fn test_ack_target_invalid_conflict() {
        let options = get_options(&["ack", "--service", "testservice", "--host", "testhost"]);
        assert_contains!(
            format!("{}", options.err().unwrap()),
            "--service <SERVICE>' cannot be used with '--host <HOST>"
        );
    }

    #[test]
    fn test_ack_target_invalid_missing() {
        let options = get_options(&["ack"]);
        assert_contains!(format!("{}", options.err().unwrap()), "the following required arguments were not provided:\n  <--host <HOST>|--service <SERVICE>");
    }

    #[test]
    fn test_ack_until() {
        let options = get_options(&["ack", "--host", "testhost", "--until", "123"]).unwrap();
        assert_eq!(
            options,
            default_client_command(ClientCommand::Ack {
                target: AckTarget::Host("testhost".into()),
                until: Some(123),
                comment: None
            })
        );
    }

    #[test]
    fn test_ack_until_invalid() {
        let options = get_options(&["ack", "--host", "testhost", "--until", "123.4"]);
        assert_contains!(options.err().unwrap(), "could not parse");
    }

    #[test]
    fn test_ack_comment() {
        let options = get_options(&[
            "ack",
            "--host",
            "testhost",
            "--comment",
            "this is a comment",
        ])
        .unwrap();
        assert_eq!(
            options,
            default_client_command(ClientCommand::Ack {
                target: AckTarget::Host("testhost".into()),
                until: None,
                comment: Some("this is a comment".into())
            })
        );
    }

    #[test]
    fn test_ack_monitor() {
        let options =
            get_options(&["ack", "--host", "testhost", "--monitor-url", "testmonitor"]).unwrap();
        assert_eq!(
            options,
            Command::Client {
                url_string: Some("testmonitor".into()),
                command: ClientCommand::Ack {
                    target: AckTarget::Host("testhost".into()),
                    until: None,
                    comment: None,
                }
            }
        );
    }

    #[test]
    fn test_host_notification() {
        let options = get_options(&[
            "host-notification",
            "--host",
            "testhost",
            "--comment",
            "this is a comment",
        ])
        .unwrap();
        assert_eq!(
            options,
            default_client_command(ClientCommand::HostNotification {
                host: "testhost".into(),
                comment: "this is a comment".into()
            })
        );
    }

    #[test]
    fn test_host_notification_monitor() {
        let options = get_options(&[
            "host-notification",
            "--host",
            "testhost",
            "--comment",
            "this is a comment",
            "--monitor-url",
            "testmonitor",
        ])
        .unwrap();
        assert_eq!(
            options,
            Command::Client {
                url_string: Some("testmonitor".into()),
                command: ClientCommand::HostNotification {
                    host: "testhost".into(),
                    comment: "this is a comment".into()
                }
            }
        );
    }

    #[test]
    fn test_host_notification_invalid() {
        let options = get_options(&["host-notification", "--host", "testhost"]);
        assert_contains!(
            format!("{}", options.err().unwrap()),
            "the following required arguments were not provided"
        );
        let options = get_options(&["host-notification", "--comment", "this is a comment"]);
        assert_contains!(
            format!("{}", options.err().unwrap()),
            "the following required arguments were not provided"
        );
        let options = get_options(&["host-notification"]);
        assert_contains!(
            format!("{}", options.err().unwrap()),
            "the following required arguments were not provided"
        );
    }

    fn default_kwallet_command(command: KWalletCommand) -> Command {
        Command::KWallet {
            url_string: None,
            command,
        }
    }

    #[test]
    fn test_kwallet() {
        let options = get_options(&["kwallet", "read-password"]).unwrap();
        assert_eq!(
            options,
            default_kwallet_command(KWalletCommand::ReadPassword)
        );
        let options = get_options(&["kwallet", "write-password"]).unwrap();
        assert_eq!(
            options,
            default_kwallet_command(KWalletCommand::WritePassword)
        );
        let options = get_options(&["kwallet", "remove-password"]).unwrap();
        assert_eq!(
            options,
            default_kwallet_command(KWalletCommand::RemovePassword)
        );
        let options = get_options(&["kwallet", "remove-password", "testmonitor"]).unwrap();
        assert_eq!(
            options,
            Command::KWallet {
                url_string: Some("testmonitor".into()),
                command: KWalletCommand::RemovePassword
            }
        );
    }

    #[test]
    fn test_kwallet_invalid() {
        let options = get_options(&["kwallet"]);
        assert_contains!(
            format!("{}", options.err().unwrap()),
            "Commands:\n  read-password    \n  write-password   \n  remove-password"
        );
    }
}
