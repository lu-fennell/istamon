use std::{
    error::Error,
    ffi::CStr,
    path::PathBuf,
    sync::mpsc::{self},
    thread,
};

use anyhow::anyhow;
use chrono::{Datelike, Local, Timelike};
use clap::Parser;
use itertools::Itertools;
use qmetaobject::{
    prelude::*, qml_register_singleton_type, qtcore::core_application::QCoreApplication, QGadget,
    QMetaType, QSingletonInit, QUrl, SimpleListItem, SimpleListModel,
};

use icinga_client::types::{Host, HostState, Service, ServiceState};
use istamon::{
    cfg::{
        self, MonitorCfg, MonitorCfgMap, PasswordManager, PasswordSource, DEFAULT_CONFIG_ENTRY_NAME,
    },
    dbus_service,
    monitor_client::{self, MonitorClient, MonitorClientMap},
    monitoring_task::{self, RequestResultMap},
};
use istamon::{dbus_service::DbusServiceError, kwallet};

use istamon_qml_extras::qwindow::QWindowPointer;

use smol::{
    future::{self},
    stream::StreamExt,
    Executor,
};
use url::Url;

#[macro_use]
extern crate cstr;

#[derive(QObject, Default)]
#[allow(non_snake_case)]
struct IstamonUtil {
    base: qt_base_class!(trait QObject),

    urlFileName: qt_method!(fn(&self, url: QUrl) -> QString),
}

#[allow(non_snake_case)]
impl IstamonUtil {
    fn urlFileName(&self, qurl: QUrl) -> QString {
        let url: Option<Url> = QString::from(qurl).to_string().parse().ok();
        url.and_then(|url| url.to_file_path().ok())
            .map(|p| QString::from(&p.to_string_lossy() as &str))
            .unwrap_or_default()
    }
}

impl QSingletonInit for IstamonUtil {
    fn init(&mut self) {
        // nothing to do
    }
}

#[derive(QObject, Default)]
struct IstamonContainer {
    quit_monitoring_snd: Option<mpsc::SyncSender<()>>,

    base: qt_base_class!(trait QObject),

    get_role: qt_method!(fn(&self, role_name: QString) -> QVariant),

    last_run: qt_property!(QDateTime; NOTIFY last_run_changed),
    last_run_changed: qt_signal!(),

    critical_count: qt_property!(usize; NOTIFY critical_count_changed),
    critical_count_changed: qt_signal!(),

    critical_handled_count: qt_property!(usize; NOTIFY critical_handled_count_changed),
    critical_handled_count_changed: qt_signal!(),

    warning_count: qt_property!(usize; NOTIFY warning_count_changed),
    warning_count_changed: qt_signal!(),

    warning_handled_count: qt_property!(usize; NOTIFY warning_handled_count_changed),
    warning_handled_count_changed: qt_signal!(),

    unknown_count: qt_property!(usize; NOTIFY unknown_count_changed),
    unknown_count_changed: qt_signal!(),

    unknown_handled_count: qt_property!(usize; NOTIFY unknown_handled_count_changed),
    unknown_handled_count_changed: qt_signal!(),

    error: qt_property!(bool; NOTIFY error_changed),
    error_changed: qt_signal!(),

    host_down_count: qt_property!(usize; NOTIFY host_down_changed),
    host_down_changed: qt_signal!(),

    host_down_handled_count: qt_property!(usize; NOTIFY host_down_handled_changed),
    host_down_handled_changed: qt_signal!(),

    list: qt_property!(QPointer<SimpleListModel<IstamonListItem>>; NOTIFY list_changed WRITE set_simple_list_model),
    list_changed: qt_signal!(),

    error_lines: qt_property!(String; NOTIFY error_lines_changed),
    error_lines_changed: qt_signal!(),

    status_icon_name: qt_property!(QString; NOTIFY status_icon_name_changed),
    status_icon_name_changed: qt_signal!(),

    toggle: qt_signal!(),
    hide: qt_signal!(),
    show: qt_signal!(),

    item_count: usize,

    role_indices: Vec<(QByteArray, i32)>,
}

impl IstamonContainer {
    fn set_last_run_now(&mut self) {
        let now = Local::now();
        let date = QDate::from_y_m_d(now.year_ce().1 as i32, now.month() as i32, now.day() as i32);
        let time = QTime::from_h_m_s_ms(
            now.hour() as i32,
            now.minute() as i32,
            Some(now.second() as i32),
            None,
        );
        self.last_run = QDateTime::from_date_time_local_timezone(date, time);
        self.last_run_changed();
    }

    fn set_counts(&mut self, new_items: &[Service], hosts: &[Host]) {
        self.item_count = new_items.len();

        self.critical_count = new_items
            .iter()
            .filter(|i| i.last_hard_state == ServiceState::CRITICAL)
            .count();
        self.critical_handled_count = new_items
            .iter()
            .filter(|i| i.last_hard_state == ServiceState::CRITICAL && i.is_handled())
            .count();

        self.warning_count = new_items
            .iter()
            .filter(|i| i.last_hard_state == ServiceState::WARNING)
            .count();
        self.warning_handled_count = new_items
            .iter()
            .filter(|i| i.last_hard_state == ServiceState::WARNING && i.is_handled())
            .count();

        self.unknown_count = new_items
            .iter()
            .filter(|i| i.last_hard_state == ServiceState::UNKNOWN)
            .count();
        self.unknown_handled_count = new_items
            .iter()
            .filter(|i| i.last_hard_state == ServiceState::UNKNOWN && i.is_handled())
            .count();

        self.host_down_count = hosts
            .iter()
            .filter(|h| h.last_hard_state == HostState::DOWN)
            .count();
        self.host_down_handled_count = hosts
            .iter()
            .filter(|h| h.last_hard_state == HostState::DOWN && h.is_handled())
            .count();

        self.critical_count_changed();
        self.warning_count_changed();
        self.unknown_count_changed();
        self.critical_handled_count_changed();
        self.warning_handled_count_changed();
        self.unknown_handled_count_changed();
        self.host_down_changed();
        self.host_down_handled_changed();
    }

    fn set_new_items(
        &mut self,
        mut services: Vec<Named<Service>>,
        mut hosts: Vec<Named<Host>>,
        errors: Vec<(String, String)>,
    ) {
        // sort by "badness" of state
        hosts.sort_by(|(_, i1), (_, i2)| i1.last_hard_state.cmp(&i2.last_hard_state).reverse());
        services.sort_by(|(_, i1), (_, i2)| i1.last_hard_state.cmp(&i2.last_hard_state).reverse());
        let new_items = hosts
            .into_iter()
            .map(IstamonListItem::from)
            .chain(services.into_iter().map(IstamonListItem::from))
            .collect();
        self.list
            .as_pinned()
            .map(|l| l.borrow_mut().reset_data(new_items));
        let error_lines = errors
            .into_iter()
            .map(|(n, msg)| format!("* {n}: {msg}"))
            .join("\n");
        self.error_lines = error_lines;
        self.error_lines_changed();
    }

    fn set_error(&mut self, error: bool) {
        self.error = error;
        self.error_changed();
    }

    fn set_status_icon_name(&mut self) {
        let icon_name = if self.error {
            "istamon-icon-error.svg"
        } else if self.host_down_count - self.host_down_handled_count > 0
            || self.critical_count - self.critical_handled_count > 0
        {
            "istamon-icon-critical.svg"
        } else if self.warning_count - self.warning_handled_count > 0 {
            "istamon-icon-warning.svg"
        } else if self.unknown_count - self.unknown_handled_count > 0 {
            "istamon-icon-unknown.svg"
        } else {
            "istamon-icon-ok.svg"
        };
        let icon_name: QString = icon_name.into();
        if icon_name != self.status_icon_name {
            self.status_icon_name = icon_name.into();
            self.status_icon_name_changed();
        }
    }

    fn set_new_result(
        &mut self,
        named_services: Vec<Named<Service>>,
        named_hosts: Vec<Named<Host>>,
        errors: Vec<(String, String)>,
    ) {
        let services = named_services.iter().map(|n| n.1.clone()).collect_vec();
        let hosts = named_hosts.iter().map(|n| n.1.clone()).collect_vec();
        self.set_error(!errors.is_empty());
        self.set_counts(&services, &hosts);
        self.set_new_items(named_services, named_hosts, errors);
        self.set_last_run_now();
        self.set_status_icon_name();
    }

    fn set_simple_list_model(&mut self, lm: QPointer<SimpleListModel<IstamonListItem>>) {
        if let Some(list) = lm.as_ref() {
            self.role_indices = list
                .role_names()
                .iter()
                .map(|entry| (entry.1.to_owned(), entry.0.to_owned()))
                .collect();
        } else {
            self.role_indices = Default::default();
        }
        self.list = lm;
    }

    fn get_role(&self, role_name: QString) -> QVariant {
        self.role_indices
            .iter()
            .find(|entry| entry.0 == role_name.to_qvariant().to_qbytearray())
            .map(|entry| entry.1.to_qvariant())
            .unwrap_or_default()
    }

    pub fn start(
        &mut self,
        clients: Vec<MonitorClient>,
    ) -> std::result::Result<(), Box<dyn Error>> {
        // shut down any previously started monitoring tasks
        self.quit_monitoring_snd.take().map(|s| s.send(()));
        // set up a new "quit monitoring" signal
        let (quit_monitoring_snd, quit_monitoring_rcv) = mpsc::sync_channel::<()>(1);
        self.quit_monitoring_snd = Some(quit_monitoring_snd);

        // callback for updating monitoring results in the view
        let ptr_upd = QPointer::from(self as &Self);
        let update_results = qmetaobject::queued_callback(move |new_results: RequestResultMap| {
            let mut services: Vec<Named<Service>> = Default::default();
            let mut hosts: Vec<Named<Host>> = Default::default();
            let mut errors: Vec<(String, String)> = Default::default();
            for (n, r) in new_results {
                match r {
                    Ok((ss, hh)) => {
                        services.extend(ss.into_iter().map(|s| (n.clone(), s)));
                        hosts.extend(hh.into_iter().map(|h| (n.clone(), h)))
                    }
                    Err(e) => errors.push((n.clone(), e)),
                }
            }
            ptr_upd.as_pinned().map(|self_| {
                let mut self_mut = self_.borrow_mut();
                self_mut.set_new_result(services, hosts, errors);
            });
        });

        // set initial results
        self.set_new_result(
            vec![],
            vec![],
            vec![("".into(), "no results received yet".into())],
        );

        // run monitoring in a separate thread, using the `update_results` callback
        thread::spawn(move || {
            let executor = Executor::new();
            future::block_on(
                executor.run(
                    monitoring_task::monitoring_tasks(&executor, clients)
                        .take_while(|_| quit_monitoring_rcv.try_recv().is_err())
                        .for_each(update_results),
                ),
            );
        });

        // start dbus service for "toggle/show/hide" methods
        dbus_service::start_toggle_handler(
            self.cb0(|self_| self_.toggle()),
            self.cb0(|self_| self_.show()),
            self.cb0(|self_| self_.hide()),
        )?;

        Ok(())
    }

    fn cb0(&self, f: impl Fn(&Self) -> () + 'static) -> impl Fn(()) -> () {
        let ptr = QPointer::from(self as &Self);
        qmetaobject::queued_callback(move |()| {
            ptr.as_pinned().map(|self_| f(self_.borrow()));
        })
    }
}

#[derive(SimpleListItem, Default, Clone)]
#[allow(non_snake_case)]
struct IstamonListItem {
    pub id: String,
    pub displayName: String,
    pub hostName: String,
    pub lastCheckOutput: String,
    pub itemState: u8,
    pub handled: bool,
    pub hostReachable: bool,
    pub isService: bool,
}

impl From<Named<Host>> for IstamonListItem {
    fn from((n, h): Named<Host>) -> Self {
        IstamonListItem {
            id: h.name.clone(),
            displayName: h.display_name.clone(),
            hostName: monitor_client::fq_hostname(&n, &h.name),
            lastCheckOutput: String::new(),
            itemState: h.last_hard_state.into(),
            handled: h.is_handled(),
            hostReachable: h.last_hard_state == HostState::UP,
            isService: false,
        }
    }
}

impl From<Named<Service>> for IstamonListItem {
    fn from((n, s): Named<Service>) -> Self {
        IstamonListItem {
            id: s.name.clone(),
            displayName: s.display_name.clone(),
            hostName: monitor_client::fq_hostname(&n, &s.host_name),
            lastCheckOutput: s
                .last_check_result
                .as_ref()
                .map(|c| c.output.clone())
                .unwrap_or(String::from("n/a")),
            itemState: s.last_hard_state.into(),
            handled: s.is_handled(),
            hostReachable: s.host_is_reachable(),
            isService: true,
        }
    }
}

#[derive(SimpleListItem, Default, Clone)]
#[allow(non_snake_case)]
struct IstamonErrorItem {
    pub name: String,
    pub msg: String,
}

impl From<(String, String)> for IstamonErrorItem {
    fn from((name, msg): (String, String)) -> Self {
        Self { name, msg }
    }
}

type Named<T> = (String, T);

#[derive(QObject, Default)]
struct WindowIcon {
    base_class: qt_base_class!(trait QObject),

    window: qt_property!(QWindowPointer; WRITE set_window),

    icon_name: qt_property!(QString; WRITE set_icon_name NOTIFY icon_name_changed),
    icon_name_changed: qt_signal!(),
}

fn icon_path(icon_name: &QString) -> QString {
    format!(":/icons/{}", icon_name).into()
}

impl WindowIcon {
    fn set_window(&mut self, wp: QWindowPointer) {
        // set the icon, if we do have one
        self.window = wp;
        self.update_icon()
    }

    fn set_icon_name(&mut self, icon_name: QString) {
        // set icon if we have a window
        self.icon_name = icon_name;
        self.update_icon();
        self.icon_name_changed();
    }

    fn update_icon(&self) {
        if self.icon_name != Default::default() {
            self.window
                .as_ref()
                .map(|w| w.set_icon(icon_path(&self.icon_name)));
        }
    }
}

#[derive(QObject)]
#[allow(non_snake_case)]
struct IstamonCfg {
    cfg_file: Option<PathBuf>,
    //  - Go to config screen when there are no clients.
    //  - handle/display erros when there is at least one client.
    // we handle loading errors of the config later, when trying to start the client,
    //  thus the whole thing is wrapped in an Result
    cfgs: anyhow::Result<MonitorCfgMap>,
    password_manager: Option<Box<dyn PasswordManager>>,

    base_class: qt_base_class!(trait QObject),

    cfgUpdated: qt_signal!(),
    notification: qt_signal!(msg: QString),

    showConfigErrors: qt_method!(fn(&self) -> ()),
    isConfigured: qt_property!(bool; READ is_configured NOTIFY cfgUpdated),
    title: qt_property!(QString; NOTIFY cfgUpdated READ title),
    hasPasswordManager: qt_method!(
        fn hasPasswordManager(&self) -> bool {
            self.password_manager.is_some()
        }
    ),

    istamon: qt_property!(QPointer<IstamonContainer>; WRITE set_istamon),

    getCfg: qt_method!(
        fn getCfg(&self) -> QVariant {
            self.get_cfg().to_qvariant()
        }
    ),
    setCfg: qt_method!(fn(&mut self, cfg: CfgModel) -> ()),
    saveCfg: qt_method!(fn(&mut self) -> ()),
}

macro_rules! log_error {
    ($s:expr, $f:literal, $($e:expr),+) => {{
        {
            let msg = format!($f, $($e),+);
            log::error!("{}", msg);
            $s.notification(format!("ERROR: {}", msg).into());
        }
    }}
}

macro_rules! log_info {
    ($s:expr, $f:literal, $($e:expr),+) => {{
        {
            let msg = format!($f, $($e),+);
            eprintln!("INFO: {}", msg);
            $s.notification(msg.into());
        }
    }}
}

impl IstamonCfg {
    pub fn set_istamon(&mut self, istamon: QPointer<IstamonContainer>) {
        self.istamon = istamon;
        if let Err(e) = self.start_istamon() {
            log_error!(self, "failed to start client: {}", e);
        };
    }

    /// return the first cfg. For now, this is the only one we can edit.
    fn cfg(&self) -> Option<(&str, &MonitorCfg)> {
        match self.cfgs.as_ref() {
            Err(e) => {
                log_error!(self, "Error loading monitor configurations {}", e);
                None
            }
            Ok(cfgs) => cfgs
                .iter()
                .filter_map(|(name, c)| {
                    c.as_ref()
                        .map_err(|e| log_error!(self, "Monitor configuration error {}", e))
                        .ok()
                        .map(|c| (name.as_str(), c))
                })
                .next(),
        }
    }

    fn update_or_insert_cfg(&mut self, cfg: MonitorCfg) -> () {
        // note that all errors for cfgs are reported by qml on initialization (cf showConfigErrors)
        match &mut self.cfgs {
            Ok(cfgs) => {
                // we always update the first entry (the one that [cfg] returns)
                match cfgs.first_valid_cfg_mut() {
                    Some(old_cfg) => {
                        *old_cfg = cfg;
                    }
                    None => {
                        cfgs.insert(DEFAULT_CONFIG_ENTRY_NAME.to_string(), cfg);
                    }
                }
            }
            // reinitialize cfgs if there was only an error before
            Err(_) => {
                self.cfgs = Ok(MonitorCfgMap::from_single_cfg(
                    DEFAULT_CONFIG_ENTRY_NAME.to_string(),
                    cfg.clone(),
                ));
            }
        }
    }

    fn start_istamon(&self) -> std::result::Result<(), String> {
        let cfgs = match self.cfgs.as_ref() {
            Ok(cfgs) => cfgs,
            // bad configs are handled by qml on initialization
            Err(_) => return Ok(()),
        };
        let maybe_clients = || -> anyhow::Result<Vec<MonitorClient>> {
            let cs = MonitorClientMap::from(cfgs)
                .into_iter()
                .filter_map(|(name, r)| match r {
                    Ok(c) => Some(c),
                    Err(e) => {
                        log_error!(self, "error creating a client from cfg `{}': {e}", name);
                        None
                    }
                })
                .collect_vec();
            if cs.is_empty() {
                Err(anyhow!(
                    "Unable to configure any clients for the provided monitoring configs"
                ))
            } else {
                Ok(cs)
            }
        };
        match maybe_clients() {
            Ok(clients) => self
                .istamon
                .as_pinned()
                .map(|c| c.borrow_mut().start(clients).map_err(|e| e.to_string()))
                .unwrap_or(Err("istamon component was deleted".to_string())),
            Err(_) => Ok(()),
        }
    }

    // currently we treat a bad/invalid first config as not present.. so the config editor will override it
    pub fn is_configured(&self) -> bool {
        self.cfg().is_some()
    }

    pub fn title(&self) -> QString {
        self.cfgs
            .as_ref()
            .map(|cfgs| {
                let title_string = cfgs.iter().map(|(n, _)| n).join(" - ");
                QString::from(title_string)
            })
            .unwrap_or(QString::from("<no valid monitor configuratiokns>"))
    }

    #[allow(non_snake_case)]
    fn showConfigErrors(&self) {
        match self.cfgs.as_ref() {
            Ok(cfgs) => {
                for (n, e) in cfgs.iter_err() {
                    log_error!(self, "Bad config `{n}': {}", e);
                }
            }
            Err(e) => log_error!(self, "Error loading monitor configs: {}", e),
        }
    }

    pub fn get_cfg(&self) -> CfgModel {
        match &self.cfg() {
            Some((_, cfg)) => CfgModel::from(*cfg),
            None => {
                log_error!(self, "{}", "No valid monitor configuration found");
                Default::default()
            }
        }
    }

    #[allow(non_snake_case)]
    pub fn setCfg(&mut self, cfgModel: CfgModel) {
        let new_cfg: std::result::Result<MonitorCfg, String> = (|| {
            let ca_cert = if cfgModel.hasCaCert {
                Some(PathBuf::from(cfgModel.caCert))
            } else {
                None
            };
            let url = cfgModel.url.parse()?;
            if cfgModel.hasCredentials {
                let user = cfgModel.user;
                let password = cfgModel.password;
                let password_source = match cfgModel.savePasswordChoice.as_str() {
                    "dontsave" => PasswordSource::None,
                    "encrypted" => self
                        .password_manager
                        .as_ref()
                        .map(|p| PasswordSource::Encrypted { provider: p.name() })
                        .unwrap_or(PasswordSource::None),
                    "unencrypted" => PasswordSource::Unencrypted,
                    _ => Err(format!(
                        "Unknown password save choice: {}",
                        cfgModel.savePasswordChoice
                    ))?,
                };
                Ok(MonitorCfg::new_with_credentials(
                    url,
                    user,
                    password,
                    password_source,
                    ca_cert,
                ))
            } else {
                Ok(MonitorCfg::new_without_credentials(url, ca_cert))
            }
        })();
        match new_cfg {
            Ok(cfg) => {
                self.update_or_insert_cfg(cfg);
                match self.start_istamon() {
                    Ok(()) => self.cfgUpdated(),
                    Err(e) => log_error!(self, "Error staring client: {}", e),
                }
            }
            Err(e) => log_error!(self, "Error setting config: {}", e),
        }
    }

    #[allow(non_snake_case)]
    pub fn saveCfg(&mut self) {
        let result: anyhow::Result<()> = (|| {
            if let Some(cfg_path) = &self.cfg_file {
                let (name, cfg) = self
                    .cfg()
                    .ok_or(anyhow!("No valid monitor configuration found"))?;
                cfg::save_cfg_in_toml_file(
                    cfg_path,
                    if name.is_empty() {
                        DEFAULT_CONFIG_ENTRY_NAME
                    } else {
                        name
                    },
                    cfg,
                )?;
                log_info!(
                    self,
                    "Successfully saved config to {}",
                    cfg_path.to_string_lossy()
                );
                match (cfg.password_source(), cfg.credentials()) {
                    (PasswordSource::Encrypted { .. }, Some((user, Some(password)))) => {
                        self.password_manager
                            .as_ref()
                            .map(|p| p.store_password(cfg.url().host_str(), user, password))
                            .unwrap_or(Err(anyhow!(
                                "No password manager loaded. This is unexpected."
                            )))?;
                    }
                    _ => {}
                }
            } else {
                Err(anyhow!("Don't know were to save the config"))?
            }
            Ok(())
        })();

        if let Err(e) = result {
            log_error!(self, "Error saving config: {}", e);
        }
    }
}

impl Default for IstamonCfg {
    #[allow(non_snake_case)]
    fn default() -> Self {
        let password_manager = kwallet::open();
        let opts = Opts::parse();
        let cfg_file = cfg::default_config_file();
        let mk_cfgs = |urls: Vec<String>| -> anyhow::Result<MonitorCfgMap> {
            let cfgs = cfg::load_monitor_config_entries_or_empty(cfg_file.clone())?;

            let loader = password_manager
                .as_ref()
                .ok()
                .map(|l| l as &dyn PasswordManager);
            Ok(MonitorCfgMap::from_urls(
                loader,
                &cfgs,
                &urls,
                cfg::DEFAULT_CONFIG_ENTRY_NAME,
            ))
        };
        let cfgs = mk_cfgs(opts.urls);

        Self {
            cfg_file,
            cfgs,
            // defaults
            title: Default::default(),
            istamon: Default::default(),
            password_manager: password_manager
                .ok()
                .map::<Box<dyn PasswordManager>, _>(|l| Box::new(l)),
            base_class: Default::default(),
            cfgUpdated: Default::default(),
            isConfigured: Default::default(),
            hasPasswordManager: Default::default(),
            getCfg: Default::default(),
            setCfg: Default::default(),
            saveCfg: Default::default(),
            notification: Default::default(),
            showConfigErrors: Default::default(),
        }
    }
}

#[derive(Clone, Default, QGadget)]
#[allow(non_snake_case, dead_code)]
struct CfgModel {
    url: qt_property!(String),
    hasCredentials: qt_property!(bool),
    user: qt_property!(String),
    password: qt_property!(String),
    savePasswordChoice: qt_property!(String),
    hasCaCert: qt_property!(bool),
    caCert: qt_property!(String),
}

#[allow(non_snake_case)]
impl From<&MonitorCfg> for CfgModel {
    fn from(cfg: &MonitorCfg) -> Self {
        let credentials = cfg.credentials().clone();
        let ca_cert = cfg.ca_cert().clone();
        let savePasswordChoice = match cfg.password_source() {
            cfg::PasswordSource::None => "dontsave".to_string(),
            cfg::PasswordSource::Encrypted { provider: _ } => "encrypted".to_string(),
            cfg::PasswordSource::Unencrypted => "unencrypted".to_string(),
        };
        CfgModel {
            url: cfg.url().get().to_string(),
            hasCredentials: credentials.is_some(),
            user: credentials
                .as_ref()
                .map(|c| c.0.clone())
                .unwrap_or_default(),
            password: credentials.and_then(|c| c.1.clone()).unwrap_or_default(),
            savePasswordChoice,
            hasCaCert: ca_cert.is_some(),
            caCert: ca_cert
                .map(|p| p.to_string_lossy().to_string())
                .unwrap_or_default(),
        }
    }
}

#[derive(Parser)]
struct Opts {
    urls: Vec<String>,
    #[clap(long, group = "startup")]
    toggle: bool,
    #[clap(long, group = "startup")]
    new_instance: bool,
}

const QML_URI: &'static CStr = cstr!("istamon.internal");
const QML_MAJOR_VERSION: u32 = 1;
const QML_MINOR_VERSION: u32 = 0;

fn main() -> anyhow::Result<()> {
    env_logger::builder()
        .filter_level(log::LevelFilter::Warn)
        .parse_default_env()
        .init();
    let opts = Opts::parse();
    if !opts.new_instance {
        let r = if opts.toggle {
            dbus_service::invoke_toggle()
        } else {
            dbus_service::invoke_show()
        };
        match r {
            Ok(()) => {
                eprint!("INFO - Istamon is already running.");
                return Ok(());
            }
            Err(DbusServiceError::DbusError(e)) => {
                // report error
                return Err(anyhow!(
                "Unexpected error when trying to detect a running istamon instance: {e}\n Use --new-instance to start a new istamon instance anyway."
            ));
            }
            Err(DbusServiceError::ServiceNotFound) => {
                // do nothing, let the app start
            }
        }
    }

    qrc!(register_icons,
    "icons" {
        "istamon-icon.svg",
        "istamon-icon-ok.svg",
        "istamon-icon-warning.svg",
        "istamon-icon-critical.svg",
        "istamon-icon-error.svg",
        "istamon-icon-unknown.svg",
    });
    register_icons();

    #[cfg(not(feature = "load-qml-from-cwd"))]
    let main_qml = {
        qrc!(register_qml_files,
        "qml" {
            "main.qml",
        });
        register_qml_files();
        ":/qml/main.qml"
    };

    #[cfg(feature = "load-qml-from-cwd")]
    let main_qml = "./main.qml";

    qml_register_type::<IstamonCfg>(
        QML_URI,
        QML_MAJOR_VERSION,
        QML_MINOR_VERSION,
        cstr!("IstamonCfg"),
    );
    qml_register_type::<IstamonContainer>(
        QML_URI,
        QML_MAJOR_VERSION,
        QML_MINOR_VERSION,
        cstr!("IstamonContainer"),
    );
    qml_register_type::<SimpleListModel<IstamonErrorItem>>(
        QML_URI,
        QML_MAJOR_VERSION,
        QML_MINOR_VERSION,
        cstr!("IstamonErrorListModel"),
    );
    qml_register_type::<SimpleListModel<IstamonListItem>>(
        QML_URI,
        QML_MAJOR_VERSION,
        QML_MINOR_VERSION,
        cstr!("IstamonListModel"),
    );
    qml_register_type::<WindowIcon>(
        QML_URI,
        QML_MAJOR_VERSION,
        QML_MINOR_VERSION,
        cstr!("WindowIcon"),
    );
    qml_register_singleton_type::<IstamonUtil>(
        QML_URI,
        QML_MAJOR_VERSION,
        QML_MINOR_VERSION,
        cstr!("IstamonUtil"),
    );

    QCoreApplication::set_organization_name("lu-fennell".into());
    QCoreApplication::set_organization_domain("de.lu-fennell".into());

    let mut engine = QmlEngine::new();
    engine.load_file(main_qml.into());
    engine.exec();
    Ok(())
}
