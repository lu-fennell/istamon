use std::{collections::BTreeMap, time::Duration};

use icinga_client::client::{self, Client};
use thiserror::Error;

use crate::cfg::{MonitorCfg, MonitorCfgError, MonitorCfgMap, MonitorCfgs, PasswordManager};

pub struct MonitorClient {
    cfg_name: String,
    client: Client,
    poll_interval: Duration,
}

impl MonitorClient {
    pub fn poll_interval(&self) -> Duration {
        self.poll_interval
    }
}

#[derive(Debug, Error)]
pub enum RequestError {
    #[error(transparent)]
    RestError(#[from] client::Error),
}

#[derive(Debug, Error)]
pub enum CfgError {
    #[error(transparent)]
    IcingaClientError(#[from] client::Error),
    #[error("{0}")]
    CfgError(String),
    #[error("Unable to configure any clients for the provided monitoring servers")]
    NoClientsFound,
}

impl MonitorClient {
    pub fn from_config<S: Into<String>>(
        name: S,
        cfg: MonitorCfg,
    ) -> Result<MonitorClient, CfgError> {
        let poll_interval = cfg.poll_interval();
        Ok(Self {
            cfg_name: name.into(),
            client: cfg.into_client()?,
            poll_interval,
        })
    }
    pub fn name(&self) -> &str {
        &self.cfg_name
    }

    pub fn get_hosts(&self) -> Result<Vec<icinga_client::types::Host>, RequestError> {
        Ok(self.client.get_hosts()?)
    }
    pub fn get_services(&self) -> Result<Vec<icinga_client::types::Service>, RequestError> {
        Ok(self.client.get_services()?)
    }
}

pub fn fq_hostname(monitor_name: impl AsRef<str>, host_name: impl AsRef<str>) -> String {
    format!("{}@{}", host_name.as_ref(), monitor_name.as_ref())
}

/// A map from names to [MonitorClient]s that possibly have failed to be configured.
///
/// a like [MonitorCfgMap], a [MonitorClientMap] is never empty, cf also [MonitorClientMap::from_urls]
pub struct MonitorClientMap {
    map: BTreeMap<String, Result<MonitorClient, CfgError>>,
}

impl MonitorClientMap {
    pub fn from_urls<S: Into<String> + AsRef<str>>(
        loader: Option<&dyn PasswordManager>,
        monitor_cfgs: &MonitorCfgs,
        urls: &[S],
        default_monitor: &str,
    ) -> Self {
        let map = MonitorCfgMap::from_urls(loader, monitor_cfgs, urls, default_monitor)
            .iter()
            .map(cfg_to_client)
            .collect();
        Self { map }
    }

    pub fn into_iter(self) -> impl Iterator<Item = (String, Result<MonitorClient, CfgError>)> {
        self.map.into_iter()
    }
    pub fn into_iter_ok(self) -> impl Iterator<Item = (String, MonitorClient)> {
        self.map.into_iter().filter_map(|(n, r)| match r {
            Ok(cfg) => Some((n, cfg)),
            Err(_) => None,
        })
    }

    pub fn iter(&self) -> impl Iterator<Item = (&String, &Result<MonitorClient, CfgError>)> {
        self.map.iter()
    }

    pub fn iter_ok(&self) -> impl Iterator<Item = (&String, &MonitorClient)> {
        self.map.iter().filter_map(|(n, r)| match r {
            Ok(cfg) => Some((n, cfg)),
            Err(_) => None,
        })
    }

    pub fn iter_err(&self) -> impl Iterator<Item = (&String, &CfgError)> {
        self.map.iter().filter_map(|(n, r)| match r {
            Ok(_) => None,
            Err(e) => Some((n, e)),
        })
    }

    pub fn first_valid_client_mut(&mut self) -> Option<&mut MonitorClient> {
        self.map.values_mut().filter_map(|r| r.as_mut().ok()).next()
    }
}

impl From<&MonitorCfgMap> for MonitorClientMap {
    fn from(value: &MonitorCfgMap) -> Self {
        let map = value.iter().map(cfg_to_client).collect();
        Self { map }
    }
}

fn cfg_to_client(
    (name, c): (&String, &Result<MonitorCfg, MonitorCfgError>),
) -> (String, Result<MonitorClient, CfgError>) {
    (
        name.clone(),
        c.as_ref()
            .map_err(|e| CfgError::CfgError(e.to_string()))
            .and_then(|cfg| MonitorClient::from_config(name, cfg.clone())),
    )
}
