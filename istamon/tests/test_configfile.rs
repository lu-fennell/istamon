use std::{env, fs};

use anyhow::{anyhow, Context};
use assertables::assert_contains;
use indoc::indoc;
use istamon::cfg::{self, MonitorCfg};
use pretty_assertions::assert_eq;
use temp_dir::TempDir;

fn setup() -> anyhow::Result<TempDir> {
    let tmpdir = TempDir::with_prefix("istamon-test_configfile")?;
    // SAFETY: we are running an integration test with a single test case
    //   so we should not have concurrent read/writes to XDG_CONFIG_HOME
    env::set_var("XDG_CONFIG_HOME", format!("{}", tmpdir.path().display()));
    Ok(tmpdir)
}

#[test]
fn test_configfile() -> anyhow::Result<()> {
    let xdg_cfg_dir = setup()?;
    test_cases(|| fs::remove_dir_all(xdg_cfg_dir.path()).context("removing tempdir/XDG_CONFIG_DIR"))
}

fn cfg1() -> (String, MonitorCfg) {
    (
        "cfg1".into(),
        MonitorCfg::new_without_credentials("http://icinga-1.example.com".parse().unwrap(), None),
    )
}

fn cfg2() -> (String, MonitorCfg) {
    (
        "cfg2".into(),
        MonitorCfg::new_without_credentials("http://icinga-2.example.com".parse().unwrap(), None),
    )
}

const CFG_FILE_EXPECTED_1: &str = indoc! {r#"
    [cfg1]
    url = "http://icinga-1.example.com/"
    api = "icinga"
    poll_interval = 5
    "#};

const CFG_FILE_EXPECTED_2: &str = indoc! {r#"
    [cfg1]
    url = "http://icinga-1.example.com/"
    api = "icinga"
    poll_interval = 5

    [cfg2]
    url = "http://icinga-2.example.com/"
    api = "icinga"
    poll_interval = 5
    "#};

const CFG_FILE_EXPECTED_3: &str = indoc! {r#"
    [cfg1]
    url = "http://icinga-2.example.com/"
    api = "icinga"
    poll_interval = 5
   "#};

fn test_cases(remove_xdg_dir: impl FnOnce() -> anyhow::Result<()>) -> anyhow::Result<()> {
    let cfg_path = cfg::default_config_file().ok_or(anyhow!("Cannot get default config file"))?;
    // initial case: no config file exists and no directory exists.
    // We expect that the directory is created and an initial config written
    let (name1, cfg1) = cfg1();
    cfg::save_cfg_in_toml_file(&cfg_path, &name1, &cfg1)?;

    assert!(
        cfg_path.exists(),
        "Expected cfg_path {} to exist",
        cfg_path.display()
    );
    assert_eq!(fs::read_to_string(&cfg_path).unwrap(), CFG_FILE_EXPECTED_1);

    // update case: config file exists.
    // We expect that the new cfg is added to the old one
    let (name2, cfg2) = cfg2();
    cfg::save_cfg_in_toml_file(&cfg_path, &name2, &cfg2)?;

    assert_eq!(fs::read_to_string(&cfg_path).unwrap(), CFG_FILE_EXPECTED_2);

    // initial case, "istamon" dir exists
    fs::remove_file(&cfg_path)?;
    cfg::save_cfg_in_toml_file(&cfg_path, &name1, &cfg2)?;
    assert!(
        cfg_path.exists(),
        "Expected cfg_path {} to exist",
        cfg_path.display()
    );
    assert_eq!(fs::read_to_string(&cfg_path).unwrap(), CFG_FILE_EXPECTED_3);

    //initial case where file exists and is empty
    fs::remove_file(&cfg_path)?;
    fs::write(&cfg_path, &[])?;
    cfg::save_cfg_in_toml_file(&cfg_path, &name1, &cfg2)?;
    assert_eq!(fs::read_to_string(&cfg_path).unwrap(), CFG_FILE_EXPECTED_3);

    //error case: file exists and is invalid
    fs::remove_file(&cfg_path)?;
    fs::write(&cfg_path, "INVALID TOML FILE".as_bytes())?;
    let error_msg = format!(
        "{:#}",
        cfg::save_cfg_in_toml_file(&cfg_path, &name1, &cfg2).unwrap_err()
    );
    assert_contains!(error_msg, "TOML parse error");
    assert_contains!(error_msg, "invalid config file");

    // initial case: XDG_CONFIG_DIR does not exist
    remove_xdg_dir()?;
    cfg::save_cfg_in_toml_file(&cfg_path, &name1, &cfg2)?;
    assert!(
        cfg_path.exists(),
        "Expected cfg_path {} to exist",
        cfg_path.display()
    );
    assert_eq!(fs::read_to_string(&cfg_path).unwrap(), CFG_FILE_EXPECTED_3);
    Ok(())
}
