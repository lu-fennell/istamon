pub mod icingaweb;

use chrono::{DateTime, Local, TimeZone};
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;

use HostState::*;
use ServiceState::*;

#[derive(serde::Deserialize, serde::Serialize)]
pub struct IcingaObjectResults<T> {
    pub results: Vec<IcingaObjectResult<T>>,
}

#[derive(serde::Deserialize, serde::Serialize)]
pub struct IcingaObjectResult<T> {
    pub attrs: T,
}

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Copy, Clone, Serialize, Deserialize)]
#[serde(try_from = "f64", into = "f64")]
pub enum HostState {
    UP,
    DOWN,
}

impl TryFrom<u8> for HostState {
    type Error = String;
    fn try_from(i: u8) -> Result<Self, Self::Error> {
        match i {
            0 => Ok(UP),
            1 => Ok(DOWN),
            _ => Err(format!("Not a valid state number: {}", i)),
        }
    }
}

impl TryFrom<f64> for HostState {
    type Error = String;
    fn try_from(f: f64) -> Result<Self, Self::Error> {
        if f.fract() != 0_f64 || f < 0_f64 || f > 255_f64 {
            return Err(format!("Not a valid host state number: {}", f));
        }
        HostState::try_from(f as u8)
    }
}

impl From<HostState> for f64 {
    fn from(s: HostState) -> Self {
        u8::from(s) as f64
    }
}

impl From<HostState> for u8 {
    fn from(s: HostState) -> Self {
        match s {
            UP => 0,
            DOWN => 1,
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Serialize, Deserialize)]
#[serde(try_from = "f64", into = "f64")]
pub enum ServiceState {
    OK,
    WARNING,
    CRITICAL,
    UNKNOWN,
}

impl TryFrom<u8> for ServiceState {
    type Error = String;
    fn try_from(i: u8) -> Result<Self, Self::Error> {
        match i {
            0 => Ok(OK),
            1 => Ok(WARNING),
            2 => Ok(CRITICAL),
            3 => Ok(UNKNOWN),
            _ => Err(format!("Not a valid state number: {}", i)),
        }
    }
}

impl TryFrom<f64> for ServiceState {
    type Error = String;
    fn try_from(f: f64) -> Result<Self, Self::Error> {
        if f.fract() != 0_f64 || f < 0_f64 || f > 255_f64 {
            return Err(format!("Not a valid service state number: {}", f));
        }
        ServiceState::try_from(f as u8)
    }
}
impl From<ServiceState> for f64 {
    fn from(s: ServiceState) -> Self {
        u8::from(s) as f64
    }
}

impl From<ServiceState> for u8 {
    fn from(s: ServiceState) -> Self {
        match s {
            OK => 0,
            WARNING => 1,
            CRITICAL => 2,
            UNKNOWN => 3,
        }
    }
}

#[derive(Debug, PartialEq, PartialOrd, Copy, Clone, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Timestamp {
    icinga_timestamp: f64,
}

impl Timestamp {
    pub fn new(icinga_timestamp: f64) -> Self {
        Timestamp { icinga_timestamp }
    }

    pub fn zero() -> Self {
        Timestamp {
            icinga_timestamp: 0_f64,
        }
    }

    /// Get a DateTime of the timestamp.
    pub fn datetime<T: TimeZone>(&self, tz: &T) -> DateTime<T> {
        let nanos = self.icinga_timestamp.fract() * 1_000_000_000_f64;
        tz.timestamp_opt(self.icinga_timestamp.trunc() as i64, nanos as u32)
            .single()
            .expect("Got an invalid timestamp from icinga")
    }

    pub fn localtime(&self) -> DateTime<Local> {
        self.datetime(&Local)
    }
}

pub trait CheckedObject {
    const PATH: &'static str;
    const OBJECT_TYPE_FOR_QUERY: &'static str;

    fn name(&self) -> &str;
    fn is_ok(&self) -> bool;
    fn acknowledgement(&self) -> &Acknowledgement;
    fn is_handled(&self) -> bool;

    fn set_name(&mut self, name: String);
    fn set_acknowledgement(&mut self, ack: Acknowledgement);
    fn set_handled(&mut self, handled: bool);
}

#[derive(Debug, PartialEq, PartialOrd, Clone, Serialize, Deserialize)]
pub struct Host {
    pub name: String,
    pub display_name: String,
    pub address: String,
    pub address6: String,
    // TODO: use hard state everywhere
    pub state: HostState,
    pub last_state: HostState,
    pub last_hard_state: HostState,
    pub last_state_change: Timestamp,
    pub last_state_up: Timestamp,
    pub last_state_down: Timestamp,

    pub next_check: Timestamp,
    pub last_check_result: Option<LastCheckResult>,

    #[serde(flatten)]
    pub acknowledgement: Acknowledgement,

    pub handled: bool,
}

impl Host {
    ///
    /// Return `true` if the Host is *handled*.
    ///
    /// A host is *handled* if it is either
    ///
    /// - acknowledged
    /// - on downtime
    /// - dependent on a handled service or host
    ///
    /// The returned value corresponds to the `handled` field in icinga's JSON representation for
    /// hosts
    pub fn is_handled(&self) -> bool {
        self.handled
    }
}

impl CheckedObject for Host {
    const PATH: &'static str = "/v1/objects/hosts";
    const OBJECT_TYPE_FOR_QUERY: &'static str = "host";

    fn name(&self) -> &str {
        &self.name
    }

    fn is_ok(&self) -> bool {
        self.state == HostState::UP
    }

    fn acknowledgement(&self) -> &Acknowledgement {
        &self.acknowledgement
    }

    fn is_handled(&self) -> bool {
        self.handled
    }

    fn set_name(&mut self, name: String) {
        self.name = name
    }

    fn set_acknowledgement(&mut self, ack: Acknowledgement) {
        self.acknowledgement = ack
    }

    fn set_handled(&mut self, handled: bool) {
        self.handled = handled
    }
}

#[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Clone, Serialize, Deserialize)]
pub struct LastCheckResult {
    pub output: String,
}

impl From<String> for LastCheckResult {
    fn from(s: String) -> Self {
        LastCheckResult { output: s }
    }
}

#[derive(Debug, PartialEq, PartialOrd, Clone, Serialize, Deserialize)]
pub struct Service {
    #[serde(rename = "__name")]
    pub name: String,
    pub display_name: String,
    pub host_name: String,
    pub state: ServiceState,
    pub last_state: ServiceState,
    pub last_hard_state: ServiceState,
    pub last_state_change: Timestamp,
    pub last_state_critical: Timestamp,
    pub last_state_warning: Timestamp,
    pub last_state_ok: Timestamp,
    pub last_state_unknown: Timestamp,

    pub next_check: Timestamp,
    pub last_check_result: Option<LastCheckResult>,

    #[serde(flatten)]
    pub acknowledgement: Acknowledgement,

    pub handled: bool,
    pub last_reachable: bool,
}

impl Service {
    /// Return `true` if the Service is *handled*.
    ///
    /// A service is *handled* if it is either
    ///
    /// - acknowledged
    /// - on downtime
    /// - dependent on a handled service or host
    ///
    /// The returned value corresponds to the `handled` field in icinga's JSON representation for
    /// services
    pub fn is_handled(&self) -> bool {
        self.handled
    }

    pub fn host_is_reachable(&self) -> bool {
        self.last_reachable
    }
}

impl CheckedObject for Service {
    const PATH: &'static str = "/v1/objects/services";
    const OBJECT_TYPE_FOR_QUERY: &'static str = "service";

    fn name(&self) -> &str {
        &self.name
    }

    fn is_ok(&self) -> bool {
        self.state == ServiceState::OK
    }

    fn acknowledgement(&self) -> &Acknowledgement {
        &self.acknowledgement
    }

    fn is_handled(&self) -> bool {
        self.handled
    }

    fn set_name(&mut self, name: String) {
        self.name = name
    }

    fn set_acknowledgement(&mut self, ack: Acknowledgement) {
        self.acknowledgement = ack
    }

    fn set_handled(&mut self, handled: bool) {
        self.handled = handled
    }
}

#[derive(Debug, PartialEq, PartialOrd, Clone, Serialize, Deserialize)]
#[serde(from = "raw::Acknowledgement", into = "raw::Acknowledgement")]
pub struct Acknowledgement {
    pub state: AckState,
    pub last_change: Option<Timestamp>,
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub enum AckState {
    None,
    Acknowledged {
        kind: AckKind,
        expiry: Option<Timestamp>,
    },
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub enum AckKind {
    Normal,
    Sticky,
}

impl Acknowledgement {
    /// Create a fresh, None-Acknowledgement
    pub fn none() -> Self {
        Acknowledgement {
            state: AckState::None,
            last_change: None,
        }
    }

    pub fn is_acknowledged(&self) -> bool {
        self.state != AckState::None
    }
}

impl From<raw::Acknowledgement> for Acknowledgement {
    fn from(raw_ack: raw::Acknowledgement) -> Self {
        fn expiry(ts: Timestamp) -> Option<Timestamp> {
            if ts.icinga_timestamp == 0_f64 {
                None
            } else {
                Some(ts)
            }
        }
        let state = match raw_ack.state {
            raw::AckState::None => AckState::None,
            raw::AckState::Normal => AckState::Acknowledged {
                kind: AckKind::Normal,
                expiry: expiry(raw_ack.expiry),
            },
            raw::AckState::Sticky => AckState::Acknowledged {
                kind: AckKind::Sticky,
                expiry: expiry(raw_ack.expiry),
            },
        };
        Acknowledgement {
            state,
            last_change: raw_ack.last_change,
        }
    }
}

impl From<Acknowledgement> for raw::Acknowledgement {
    fn from(ack: Acknowledgement) -> Self {
        let (state, expiry) = match ack.state {
            AckState::None => (raw::AckState::None, Timestamp::zero()),
            AckState::Acknowledged {
                kind: AckKind::Normal,
                expiry,
            } => (raw::AckState::Normal, expiry.unwrap_or(Timestamp::zero())),
            AckState::Acknowledged {
                kind: AckKind::Sticky,
                expiry,
            } => (raw::AckState::Sticky, expiry.unwrap_or(Timestamp::zero())),
        };
        let last_change = ack.last_change;
        raw::Acknowledgement {
            state,
            expiry,
            last_change,
        }
    }
}

pub mod raw {
    //! Struct definitions for the `raw` variants of icinga-returned structures. These are also
    //! *serializeable* but it return are slightly more inconvenient to work with.

    use std::convert::TryFrom;

    use super::Timestamp;
    use serde::{Deserialize, Serialize};

    #[derive(Debug, PartialEq, PartialOrd, Clone, Serialize, Deserialize)]
    pub struct Acknowledgement {
        #[serde(rename = "acknowledgement")]
        pub state: AckState,
        #[serde(rename = "acknowledgement_expiry")]
        pub expiry: Timestamp,
        #[serde(rename = "acknowledgement_last_change")]
        pub last_change: Option<Timestamp>,
    }

    #[derive(Debug, PartialEq, PartialOrd, Eq, Ord, Clone, Serialize, Deserialize)]
    #[serde(try_from = "f64", into = "f64")]
    pub enum AckState {
        None,
        Normal,
        Sticky,
    }

    impl TryFrom<f64> for AckState {
        type Error = String;
        fn try_from(f: f64) -> Result<Self, Self::Error> {
            if f.fract() != 0_f64 || f < 0_f64 || f > 255_f64 {
                return Err(format!("Not a valid host state number: {}", f));
            }
            AckState::try_from(f as u8)
        }
    }

    impl From<AckState> for f64 {
        fn from(s: AckState) -> Self {
            match s {
                AckState::None => 0_f64,
                AckState::Normal => 1_f64,
                AckState::Sticky => 2_f64,
            }
        }
    }

    impl TryFrom<u8> for AckState {
        type Error = String;
        fn try_from(i: u8) -> Result<Self, Self::Error> {
            match i {
                0 => Ok(AckState::None),
                1 => Ok(AckState::Normal),
                2 => Ok(AckState::Sticky),
                _ => Err(format!("Invalid acknowledgement state number: {}", i)),
            }
        }
    }
}

#[cfg(test)]
mod test {

    #[cfg(test)]
    use pretty_assertions::assert_eq;

    use super::*;
    use chrono::Local;
    use serde_json::Value;

    static HOST_OBJ: &[u8] = include_bytes!("../test-input/example-host-object.json");
    static SERVICE_OBJ: &[u8] = include_bytes!("../test-input/example-service-object.json");

    #[test]
    fn test_timestamp_localtime() {
        let icinga_timestamp = 1584180320.814572_f64;
        assert_eq!(
            Timestamp::new(icinga_timestamp).localtime(),
            Local.timestamp_opt(1584180320, 814572095).unwrap()
        );

        assert_eq!(
            Timestamp::new(1583917754.192786_f64).localtime(),
            Local.timestamp_opt(1583917754, 192785978).unwrap()
        );

        assert_eq!(
            Timestamp::new(1583917754.192786111_f64).localtime(),
            Local.timestamp_opt(1583917754, 192786216).unwrap()
        );
    }

    #[test]
    fn test_de_host_object() {
        let r: Host = serde_json::from_slice(HOST_OBJ).unwrap();
        assert_eq!(
            r,
            Host {
                name: "example-host".to_string(),
                display_name: "example-host".to_string(),
                address: "example-host.de".to_string(),
                address6: "".to_string(),
                state: HostState::UP,
                last_state: HostState::UP,
                last_hard_state: HostState::UP,
                last_state_change: Timestamp::new(1584154326.317929_f64),
                last_state_up: Timestamp::new(1584180320.814572_f64),
                last_state_down: Timestamp::new(1584154292.773004_f64),
                next_check: Timestamp::new(1584180380.234708_f64),
                last_check_result: Some(LastCheckResult {
                    output: "PING OK - Packet loss = 0%, RTA = 23.24 ms".to_string()
                }),

                acknowledgement: Acknowledgement::none(),

                handled: false,
            }
        )
    }

    static REDUCED_HOST_OBJ: &[u8] = r#"
{
       "acknowledgement": 0.0,
       "acknowledgement_expiry": 0.0,
       "acknowledgement_last_change": null,
       "address": "example-host.de",
       "address6": "",
       "display_name": "example-host",
       "handled": false,
       "last_check_result": {
         "output": "PING OK - Packet loss = 0%, RTA = 23.24 ms"
       },
       "last_hard_state": 0.0,
       "last_state": 0.0,
       "last_state_change": 1584154326.317929,
       "last_state_down": 1584154292.773004,
       "last_state_up": 1584180320.814572,
       "name": "example-host",
       "next_check": 1584180380.234708,
       "state": 0.0
     }
"#
    .as_bytes();

    #[test]
    fn test_se_host_object() {
        let host: Host = Host {
            name: "example-host".to_string(),
            display_name: "example-host".to_string(),
            address: "example-host.de".to_string(),
            address6: "".to_string(),
            state: HostState::UP,
            last_state: HostState::UP,
            last_hard_state: HostState::UP,
            last_state_change: Timestamp::new(1584154326.317929_f64),
            last_state_up: Timestamp::new(1584180320.814572_f64),
            last_state_down: Timestamp::new(1584154292.773004_f64),
            next_check: Timestamp::new(1584180380.234708_f64),
            last_check_result: Some(LastCheckResult {
                output: "PING OK - Packet loss = 0%, RTA = 23.24 ms".to_string(),
            }),

            acknowledgement: Acknowledgement::none(),

            handled: false,
        };
        assert_eq!(
            serde_json::to_value(host).unwrap(),
            serde_json::from_slice::<Value>(REDUCED_HOST_OBJ).unwrap()
        )
    }

    #[test]
    fn test_de_service_object() {
        let r: Service = serde_json::from_slice(SERVICE_OBJ).unwrap();
        assert_eq!(
            r,
            Service {
                name: "example-host!example-service".to_string(),
                display_name: "check-example-service".to_string(),
                host_name: "example-host".to_string(),
                state: ServiceState::OK,
                last_state: ServiceState::OK,
                last_hard_state: ServiceState::OK,
                last_state_change: Timestamp::new(1583917754.192786_f64),
                last_state_critical: Timestamp::new(0_f64),
                last_state_ok: Timestamp::new(1584184342.288635_f64),
                last_state_warning: Timestamp::new(1583917693.623402_f64),
                last_state_unknown: Timestamp::new(1583860703.96808_f64),
                next_check: Timestamp::new(1584184401.578774_f64),
                last_check_result: Some(LastCheckResult {
                    output: "OK: nothing wrong".to_string()
                }),
                acknowledgement: Acknowledgement::none(),
                handled: false,
                last_reachable: true,
            }
        )
    }

    static REDUCED_SERVICE_OBJ: &[u8] = r#"
{
       "__name": "example_host!example_service",
       "acknowledgement": 0.0,
       "acknowledgement_expiry": 0.0,
       "acknowledgement_last_change": null,
       "display_name": "example_service",
       "handled": false,
       "last_check_result": {
         "output": "PING OK - Packet loss = 0%, RTA = 23.24 ms"
       },
       "host_name": "example_host",
       "last_hard_state": 0.0,
        "last_state": 0.0,
        "last_state_change": 1584154326.317929,
        "last_state_critical": 1584154292.773004,
        "last_state_warning": 1584154292.773004,
        "last_state_ok": 1584180320.814572,
        "last_state_unknown": 1584154292.773004,
        "last_reachable": true,
       "next_check": 1584180380.234708,
       "state": 0.0
     }
"#
    .as_bytes();

    #[test]
    fn test_se_service_object() {
        let service: Service = Service {
            name: "example_host!example_service".to_string(),
            host_name: "example_host".to_string(),
            display_name: "example_service".to_string(),
            state: ServiceState::OK,
            last_state: ServiceState::OK,
            last_hard_state: ServiceState::OK,
            last_state_change: Timestamp::new(1584154326.317929_f64),
            last_state_ok: Timestamp::new(1584180320.814572_f64),
            last_state_warning: Timestamp::new(1584154292.773004_f64),
            last_state_critical: Timestamp::new(1584154292.773004_f64),
            last_state_unknown: Timestamp::new(1584154292.773004_f64),
            last_reachable: true,
            next_check: Timestamp::new(1584180380.234708_f64),
            last_check_result: Some(LastCheckResult {
                output: "PING OK - Packet loss = 0%, RTA = 23.24 ms".to_string(),
            }),

            acknowledgement: Acknowledgement::none(),

            handled: false,
        };
        assert_eq!(
            serde_json::to_value(service).unwrap(),
            serde_json::from_slice::<Value>(REDUCED_SERVICE_OBJ).unwrap()
        )
    }

    static ACK_OBJ_STR: &str = r#"
        {
        "acknowledgement": 1.0,
        "acknowledgement_expiry": 0.0,
        "acknowledgement_last_change": 1614632568.699408
        }
        "#;
    static ACK_OBJ: &[u8] = ACK_OBJ_STR.as_bytes();
    #[test]
    fn test_de_ack_raw() {
        let a: raw::Acknowledgement = serde_json::from_slice(ACK_OBJ).unwrap();
        assert_eq!(
            a,
            raw::Acknowledgement {
                state: raw::AckState::Normal,
                expiry: Timestamp::zero(),
                last_change: Some(Timestamp::new(1614632568.699408f64))
            }
        );
    }
    #[test]
    fn test_se_ack_raw() {
        let a: raw::Acknowledgement = raw::Acknowledgement {
            state: raw::AckState::Normal,
            expiry: Timestamp::zero(),
            last_change: Some(Timestamp::new(1614632568.699408f64)),
        };
        assert_eq!(
            serde_json::to_value(&a).unwrap(),
            serde_json::from_slice::<Value>(ACK_OBJ).unwrap()
        );
    }
    #[test]
    fn test_de_ack() {
        let a: Acknowledgement = serde_json::from_slice(ACK_OBJ).unwrap();
        assert_eq!(
            a,
            Acknowledgement {
                last_change: Some(Timestamp::new(1614632568.699408f64)),
                state: AckState::Acknowledged {
                    kind: AckKind::Normal,
                    expiry: None
                },
            }
        );

        assert!(a.is_acknowledged(), "Should be acknowledged");
    }

    #[test]
    fn test_se_ack() {
        let a: Acknowledgement = Acknowledgement {
            last_change: Some(Timestamp::new(1614632568.699408f64)),
            state: AckState::Acknowledged {
                kind: AckKind::Normal,
                expiry: None,
            },
        };
        assert_eq!(
            serde_json::to_value(a).unwrap(),
            serde_json::from_slice::<Value>(ACK_OBJ).unwrap()
        );
    }

    static ACK_EXP_OBJ: &[u8] = r#"
        {
        "acknowledgement": 1.0,
        "acknowledgement_expiry": 1714632568.699408,
        "acknowledgement_last_change": 1614632568.699408
        }
        "#
    .as_bytes();

    #[test]
    fn test_de_ack_raw_exp() {
        let expected_expiry = Timestamp::new(1714632568.699408f64);
        let a: raw::Acknowledgement = serde_json::from_slice(ACK_EXP_OBJ).unwrap();
        assert_eq!(
            a,
            raw::Acknowledgement {
                state: raw::AckState::Normal,
                expiry: expected_expiry,
                last_change: Some(Timestamp::new(1614632568.699408f64))
            }
        );
    }

    #[test]
    fn test_de_ack_exp() {
        let expected_expiry = Timestamp::new(1714632568.699408f64);
        let a: Acknowledgement = serde_json::from_slice(ACK_EXP_OBJ).unwrap();
        assert_eq!(
            a,
            Acknowledgement {
                last_change: Some(Timestamp::new(1614632568.699408f64)),
                state: AckState::Acknowledged {
                    kind: AckKind::Normal,
                    expiry: Some(expected_expiry)
                },
            }
        );

        assert!(a.is_acknowledged(), "Should be acknowledged");
    }

    static NOT_ACK_OBJ: &[u8] = r#"
        {
        "acknowledgement": 0.0,
        "acknowledgement_expiry": 0.0,
        "acknowledgement_last_change": 1614632568.699408
        }
        "#
    .as_bytes();

    #[test]
    fn test_de_not_ack_raw() {
        let a: raw::Acknowledgement = serde_json::from_slice(NOT_ACK_OBJ).unwrap();
        assert_eq!(
            a,
            raw::Acknowledgement {
                state: raw::AckState::None,
                expiry: Timestamp::zero(),
                last_change: Some(Timestamp::new(1614632568.699408_f64))
            }
        );
    }

    #[test]
    fn test_de_not_ack() {
        let a: Acknowledgement = serde_json::from_slice(NOT_ACK_OBJ).unwrap();
        assert_eq!(
            a,
            Acknowledgement {
                state: AckState::None,
                last_change: Some(Timestamp::new(1614632568.699408_f64))
            }
        );
        assert!(!a.is_acknowledged(), "Should not be acknowledged")
    }
}
