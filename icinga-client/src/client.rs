use std::{convert::TryFrom, fs, io, path::PathBuf, str::FromStr};

pub use reqwest::blocking::RequestBuilder;
pub use reqwest::header;
use reqwest::header::HeaderValue;
pub use reqwest::Method;
use reqwest::{blocking::ClientBuilder, Certificate, StatusCode};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use url::{ParseError, Url};

use crate::types;
use crate::types::icingaweb;
use crate::types::Host;
use crate::types::IcingaObjectResults;
use crate::types::Service;

/// An http(s) URL.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
#[serde(try_from = "Url", into = "Url")]
pub struct IcingaUrl {
    url: Url,
}

/// An api choice. Plain icinga api or icingaweb's api.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum Api {
    #[serde(rename = "icinga")]
    Icinga,
    #[serde(rename = "icingaweb")]
    IcingaWeb,
}

impl Default for Api {
    fn default() -> Self {
        Api::Icinga
    }
}

impl IcingaUrl {
    pub fn get(&self) -> &Url {
        &self.url
    }

    pub fn take(self) -> Url {
        self.url
    }

    pub fn host_str(&self) -> &str {
        // [IcingaUrl]s always have a host
        self.url.host_str().unwrap()
    }

    pub fn host(&self) -> url::Host<&str> {
        // [IcingaUrl]s always have a host
        self.url.host().unwrap()
    }

    pub fn split_credentials(&self) -> (Option<Credentials>, Self) {
        let stripped = url_without_credentials(&self.url);
        let user = percent_encoding::percent_decode_str(self.url.username()).decode_utf8_lossy();
        let password = self
            .url
            .password()
            .map(|p| percent_encoding::percent_decode_str(p).decode_utf8_lossy());
        let credentials = if user.is_empty() && password.is_none() {
            None
        } else {
            Some((user.to_string(), password.map(|p| p.to_string())))
        };

        (credentials, Self { url: stripped })
    }

    pub fn set_username(&mut self, user: &str) {
        // does not panic: url is not a mailto-link
        self.url.set_username(user).unwrap()
    }

    pub fn set_password(&mut self, password: Option<&str>) {
        // does not panic: url always has a base
        self.url.set_password(password).unwrap()
    }
}

impl TryFrom<Url> for IcingaUrl {
    type Error = String;

    fn try_from(url: Url) -> std::result::Result<Self, Self::Error> {
        let scheme = url.scheme();
        if scheme == "http" || scheme == "https" {
            Ok(IcingaUrl { url })
        } else {
            Err(format!(
                r#"Illegal url scheme "{}:" in url "{}". Only schemes "http(s)" are allowed."#,
                scheme,
                url.as_str()
            ))
        }
    }
}

impl From<IcingaUrl> for Url {
    fn from(u: IcingaUrl) -> Self {
        u.take()
    }
}

impl FromStr for IcingaUrl {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let url = Url::from_str(s).map_err(|e| format!("Url '{}' is invalid: {}", s, e))?;
        IcingaUrl::try_from(url)
    }
}

fn url_without_credentials(url: &Url) -> Url {
    let mut result: Url = url.clone();
    result.set_username("").unwrap_or(());
    result.set_password(Option::None).unwrap_or(());

    result
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("{0}")]
    ReqwestError(#[from] reqwest::Error),
    #[error("{0}")]
    SerdeError(#[from] serde_json::Error),
    #[error("{0}")]
    IcingaError(String),
    #[error("{0}")]
    CertReadError(#[from] io::Error),
    #[error("Unauthorized")]
    Unauthorized,
}

pub type Result<T> = std::result::Result<T, Error>;
pub type Credentials = (String, Option<String>);

/// A blocking icinga API client
pub struct Client {
    client: reqwest::blocking::Client,
    api: Api,
    base_uri: Url,
    credentials: Option<Credentials>,
}

impl Client {
    /// Create a new client with [url] as base uri. If provided, uses the given certificate to
    /// authenticate the server.
    pub fn new(
        url: IcingaUrl,
        api: Api,
        maybe_cert_path: Option<PathBuf>,
        credentials: Option<Credentials>,
    ) -> Result<Client> {
        let client_builder = ClientBuilder::new();
        let client = if let Some(cert_path) = maybe_cert_path {
            let cert_content: Vec<u8> = fs::read(cert_path).map_err(Error::CertReadError)?;
            let cert = Certificate::from_pem(&cert_content)?;
            client_builder
                .add_root_certificate(cert)
                .danger_accept_invalid_hostnames(true)
                .tls_built_in_root_certs(false)
                .build()
        } else {
            client_builder.build()
        }?;
        Ok(Client {
            client,
            api,
            base_uri: url.take(),
            credentials,
        })
    }

    /// Send a request to the icinga API as specified by the given [RequestBuilder].
    ///
    /// This method adds credentials and an `application/json` ACCEPT header to `request`, sends it
    /// with the underlying [Client] and deserializes the response.
    pub fn send_request<T: serde::de::DeserializeOwned>(
        &self,
        request: RequestBuilder,
    ) -> Result<T> {
        send_request_with_credentials(&self.credentials, request)
    }

    /// Get a [RequestBuilder] from the underlying [Client] for the given `path` under
    /// `self.base_uri`
    pub fn request(
        &self,
        method: reqwest::Method,
        path: &str,
    ) -> std::result::Result<RequestBuilder, ParseError> {
        Ok(self
            .client
            .request(method, join_path(&self.base_uri, path)?))
    }

    pub fn get_hosts(&self) -> Result<Vec<Host>> {
        match self.api {
            Api::Icinga => self.get_hosts_impl::<IcingaMethods>(),
            Api::IcingaWeb => self.get_hosts_impl::<IcingaWebMethods>(),
        }
    }

    fn get_hosts_impl<M: ClientMethods>(&self) -> Result<Vec<Host>> {
        let results: M::HostList =
            // we can unwrap with this constant path
            self.send_request(self.request(Method::GET, M::HOST_LIST_URL).unwrap())?;
        Ok(M::to_hosts(results))
    }

    pub fn get_services(&self) -> Result<Vec<Service>> {
        match self.api {
            Api::Icinga => self.get_services_impl::<IcingaMethods>(),
            Api::IcingaWeb => self.get_services_impl::<IcingaWebMethods>(),
        }
    }

    fn get_services_impl<M: ClientMethods>(&self) -> Result<Vec<Service>> {
        let results: M::ServiceList =
            // we can unwrap with this constant path
            self.send_request(self.request(Method::GET, M::SERVICE_LIST_URL).unwrap())?;
        Ok(M::to_services(results))
    }

    pub fn api(&self) -> &Api {
        &self.api
    }
}

trait ClientMethods {
    type HostList: DeserializeOwned;
    type ServiceList: DeserializeOwned;

    const HOST_LIST_URL: &'static str;
    const SERVICE_LIST_URL: &'static str;

    fn to_hosts(h: Self::HostList) -> Vec<types::Host>;
    fn to_services(h: Self::ServiceList) -> Vec<types::Service>;
}

struct IcingaMethods;
impl ClientMethods for IcingaMethods {
    type HostList = IcingaObjectResults<Host>;
    type ServiceList = IcingaObjectResults<Service>;

    const HOST_LIST_URL: &'static str = "/v1/objects/hosts";
    const SERVICE_LIST_URL: &'static str = "/v1/objects/services";

    fn to_hosts(h: Self::HostList) -> Vec<types::Host> {
        let result_vec = h.results;
        result_vec.into_iter().map(|r| r.attrs).collect()
    }

    fn to_services(h: Self::ServiceList) -> Vec<types::Service> {
        let result_vec = h.results;
        result_vec.into_iter().map(|r| r.attrs).collect()
    }
}

struct IcingaWebMethods;
impl ClientMethods for IcingaWebMethods {
    type HostList = Vec<icingaweb::Host>;
    type ServiceList = Vec<icingaweb::Service>;

    const HOST_LIST_URL: &'static str = "/monitoring/list/hosts";
    const SERVICE_LIST_URL: &'static str = "/monitoring/list/services";

    fn to_hosts(h: Self::HostList) -> Vec<types::Host> {
        h.into_iter().map(|h| Host::try_from(h).unwrap()).collect()
    }

    fn to_services(h: Self::ServiceList) -> Vec<types::Service> {
        h.into_iter()
            .map(|h| Service::try_from(h).unwrap())
            .collect()
    }
}

fn send_request_with_credentials<T: serde::de::DeserializeOwned>(
    credentials: &Option<Credentials>,
    request: RequestBuilder,
) -> Result<T> {
    let request = if let Some((api_name, api_pass)) = credentials {
        request.basic_auth(api_name, api_pass.as_ref())
    } else {
        request
    };
    let response = request.header(header::ACCEPT, "application/json").send()?;

    let status = response.status();
    if status.is_success() {
        let content_type = response.headers().get("Content-Type").cloned();
        if content_type == Some(HeaderValue::from_str("application/json").unwrap()) {
            Ok(response.json::<T>()?)
        } else {
            let body_start = response
                .text()
                .map(|s| {
                    if s.len() <= 100 {
                        s
                    } else {
                        format!("{}...", s.chars().take(100).collect::<String>())
                    }
                })
                .unwrap_or_else(|e| format!("<error: {}>", e.to_string()));
            Err(Error::IcingaError(format!("Expected json response. Got content-type {content_type:?}. First 100 chars of body:\n {body_start}.")))
        }
    } else if status == StatusCode::UNAUTHORIZED {
        Err(Error::Unauthorized)
    } else {
        let body = response.text()?;
        Err(Error::IcingaError(format!(
            "ERROR {} {} {}",
            status.as_str(),
            status.canonical_reason().unwrap_or(""),
            body
        )))?
    }
}

fn join_path(url: &Url, path: &str) -> std::result::Result<Url, ParseError> {
    let mut result: Url = url.clone();
    if !result.path().ends_with("/") {
        let relative_original_path: String = result.path().chars().chain("/".chars()).collect();
        result.set_path(&relative_original_path);
    }
    result.join(path.trim_start_matches("/"))
}

#[cfg(test)]
mod test {
    use serde_json::Value;

    use super::*;

    #[test]
    fn test_base_uri_with_appended_path_from_url() {
        assert_eq!(
            join_path(
                &Url::parse("https://example.com:8443/hello/world?what=42").unwrap(),
                "/and/then/some?queries=42"
            )
            .unwrap(),
            Url::parse("https://example.com:8443/hello/world/and/then/some?queries=42").unwrap()
        );
        assert_eq!(
            join_path(
                &Url::parse("https://example.com:8443").unwrap(),
                "/and/then/some?queries=42"
            )
            .unwrap(),
            Url::parse("https://example.com:8443/and/then/some?queries=42").unwrap()
        );
    }

    #[test]
    fn test_strip_credentials_no_username_but_password() {
        let url: IcingaUrl = "http://:mutti123@example.com".parse().unwrap();
        assert_eq!(
            url.split_credentials(),
            (
                Some(("".to_string(), Some("mutti123".to_string()))),
                "http://example.com".parse().unwrap()
            )
        );
    }

    #[test]
    fn test_strip_credentials_username_password() {
        let url: IcingaUrl = "http://test:mutti123@example.com".parse().unwrap();
        assert_eq!(
            url.split_credentials(),
            (
                Some(("test".to_string(), Some("mutti123".to_string()))),
                "http://example.com".parse().unwrap()
            )
        );
    }

    #[test]
    fn test_strip_credentials_nothing() {
        let url: IcingaUrl = "http://example.com".parse().unwrap();
        assert_eq!(
            url.split_credentials(),
            (None, "http://example.com".parse().unwrap())
        );
    }

    #[test]
    fn test_strip_credentials_only_username() {
        let url: IcingaUrl = "http://user@example.com".parse().unwrap();
        assert_eq!(
            url.split_credentials(),
            (
                Some(("user".to_string(), None)),
                "http://example.com".parse().unwrap()
            )
        );
    }

    #[test]
    fn test_strip_credentials_empty_username_and_password() {
        let url: IcingaUrl = "http://:@example.com".parse().unwrap();
        // Note: this case is the same as using no auth at all; doing it like this seems like the
        // best solution, as [Url] does not distinguish between empty password and no password
        assert_eq!(
            url.split_credentials(),
            (None, "http://example.com".parse().unwrap())
        );
    }

    #[test]
    fn test_icinga_url_ok() {
        let url = Url::parse("https://user:pass@example.com:8443").unwrap();
        assert_eq!(
            IcingaUrl::try_from(url.clone()),
            Ok(IcingaUrl { url: url.clone() })
        );

        let url = Url::parse("http://user:pass@example.com:8443").unwrap();
        assert_eq!(
            IcingaUrl::try_from(url.clone()),
            Ok(IcingaUrl { url: url.clone() })
        );

        let url = Url::parse("http://example.com").unwrap();
        assert_eq!(
            IcingaUrl::try_from(url.clone()),
            Ok(IcingaUrl { url: url.clone() })
        );
    }

    #[test]
    fn test_icinga_url_err() {
        let url = Url::parse("unix:/run/foo.socket").unwrap();
        assert_eq!(
            IcingaUrl::try_from(url.clone()).err().unwrap().to_string(),
            r#"Illegal url scheme "unix:" in url "unix:/run/foo.socket". Only schemes "http(s)" are allowed."#
        );
    }

    #[test]
    fn test_icinga_url_serde() {
        let url: IcingaUrl = "http://user:pass@example.com:8443".parse().unwrap();
        let value: Value = "http://user:pass@example.com:8443/".into();
        assert_eq!(serde_json::to_value(url.clone()).unwrap(), value);

        let parsed_url: IcingaUrl = serde_json::from_value(value).unwrap();
        assert_eq!(parsed_url, url)
    }

    #[test]
    fn test_without_credentials() {
        assert_eq!(
            url_without_credentials(
                &Url::parse("https://user:pass@example.com:8443/hello/world?what=42").unwrap()
            )
            .as_str(),
            "https://example.com:8443/hello/world?what=42"
        );
        assert_eq!(
            url_without_credentials(
                &Url::parse("https://example.com:8443/hello/world?what=42").unwrap()
            )
            .as_str(),
            "https://example.com:8443/hello/world?what=42"
        );
    }
}
