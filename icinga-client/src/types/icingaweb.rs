use std::convert::TryFrom;

use serde::{Deserialize, Serialize};

use crate::types::{AckKind, AckState, Acknowledgement, LastCheckResult};

use super::{HostState, ServiceState, Timestamp};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Host {
    host_icon_image: String,
    host_icon_image_alt: String,
    host_name: String,
    host_display_name: String,
    host_state: String,
    host_acknowledged: String,
    host_output: String,
    host_attempt: String,
    host_in_downtime: String,
    host_is_flapping: String,
    host_state_type: String,
    host_handled: String,
    host_last_state_change: Option<String>,
    host_notifications_enabled: String,
    host_active_checks_enabled: String,
    host_passive_checks_enabled: String,
    host_check_command: String,
    host_next_update: String,
}

impl TryFrom<Host> for super::Host {
    type Error = String;

    fn try_from(value: Host) -> Result<Self, Self::Error> {
        let state =
            HostState::try_from(value.host_state.parse::<u8>().map_err(|e| e.to_string())?)?;
        let last_state_change = string_to_last_state_change(value.host_last_state_change)?;
        let next_check = string_to_ts(value.host_next_update)?;
        let last_check_result = Some(LastCheckResult::from(value.host_output));
        let handled = string_to_bool(value.host_handled);
        let acknowledgement = flag_to_acknowlegdement(string_to_bool(value.host_acknowledged));

        Ok(Self {
            name: value.host_name,
            display_name: value.host_display_name,
            state,
            last_state: state,
            last_hard_state: state,
            last_state_change,
            next_check,
            last_check_result,
            handled,
            acknowledgement,

            address: "n/a".into(),
            address6: "n/a".into(),
            last_state_up: Timestamp::zero(),
            last_state_down: Timestamp::zero(),
        })
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Service {
    host_name: String,
    host_display_name: String,
    host_state: String,
    service_description: String,
    service_display_name: String,
    service_state: String,
    service_in_downtime: String,
    service_acknowledged: String,
    service_handled: String,
    service_output: String,
    service_perfdata: String,
    service_attempt: String,
    service_last_state_change: Option<String>,
    service_icon_image: String,
    service_icon_image_alt: String,
    service_is_flapping: String,
    service_state_type: String,
    service_severity: String,
    service_notifications_enabled: String,
    service_active_checks_enabled: String,
    service_passive_checks_enabled: String,
    service_check_command: String,
    service_next_update: String,
}

impl TryFrom<Service> for super::Service {
    type Error = String;

    fn try_from(value: Service) -> Result<Self, Self::Error> {
        let state = ServiceState::try_from(
            value
                .service_state
                .parse::<u8>()
                .map_err(|e| e.to_string())?,
        )?;
        Ok(Self {
            name: format!("{}!{}", value.host_name, value.service_description),
            display_name: value.service_display_name,
            host_name: value.host_name,
            state,
            last_state: state,
            last_hard_state: state,
            last_state_change: string_to_last_state_change(value.service_last_state_change)?,
            next_check: string_to_ts(value.service_next_update)?,
            last_check_result: Some(LastCheckResult::from(value.service_output)),
            acknowledgement: flag_to_acknowlegdement(string_to_bool(value.service_acknowledged)),
            handled: string_to_bool(value.service_handled),

            last_state_critical: Timestamp::zero(),
            last_state_warning: Timestamp::zero(),
            last_state_ok: Timestamp::zero(),
            last_state_unknown: Timestamp::zero(),
            last_reachable: true,
        })
    }
}

fn string_to_ts(s: String) -> Result<Timestamp, String> {
    Ok(Timestamp::new(s.parse::<f64>().map_err(|e| e.to_string())?))
}

fn string_to_bool(s: String) -> bool {
    s == "1"
}
fn flag_to_acknowlegdement(f: bool) -> Acknowledgement {
    Acknowledgement {
        state: if f {
            AckState::Acknowledged {
                kind: AckKind::Normal,
                expiry: None,
            }
        } else {
            AckState::None
        },
        last_change: None,
    }
}

fn string_to_last_state_change(maybe_string: Option<String>) -> Result<Timestamp, String> {
    if let Some(s) = maybe_string {
        Ok(string_to_ts(s)?)
    } else {
        Ok(Timestamp::zero())
    }
}

#[cfg(test)]
mod test {
    use crate::types::{
        Acknowledgement, Host, HostState, LastCheckResult, Service, ServiceState, Timestamp,
    };

    use super::*;

    #[cfg(test)]
    use pretty_assertions::assert_eq;

    static HOST_RESP: &[u8] =
        include_bytes!("../../test-input/example-icingaweb-hosts-response.json");
    static SERVICE_RESP: &[u8] =
        include_bytes!("../../test-input/example-icingaweb-services-response.json");

    #[test]
    fn test_de_hosts() {
        let hosts: Vec<Host> = vec![
            Host {
                name: "srv-dns1.icinga.com".into(),
                display_name: "DNS Server 1".into(),
                address: "n/a".into(),
                address6: "n/a".into(),
                state: HostState::UP,
                last_state: HostState::UP,
                last_hard_state: HostState::UP,
                last_state_change: Timestamp::new(1651294825 as f64),
                last_state_up: Timestamp::zero(),
                last_state_down: Timestamp::zero(),
                next_check: Timestamp::new(1651300391.00002),
                last_check_result: Some(LastCheckResult {
                    output: "PING OK - Packet loss = 0%, RTA = 2.24 ms".into(),
                }),
                acknowledgement: Acknowledgement::none(),
                handled: false,
            },
            Host {
                name: "git.icinga.com".into(),
                display_name: "Git Server".into(),
                address: "n/a".into(),
                address6: "n/a".into(),
                state: HostState::DOWN,
                last_state: HostState::DOWN,
                last_hard_state: HostState::DOWN,
                last_state_change: Timestamp::zero(),
                last_state_up: Timestamp::zero(),
                last_state_down: Timestamp::zero(),
                next_check: Timestamp::new(1651300437.00002),
                last_check_result: Some(LastCheckResult {
                    output: "PING CRITICAL - Packet loss = 100%".into(),
                }),
                acknowledgement: Acknowledgement {
                    state: AckState::Acknowledged {
                        kind: AckKind::Normal,
                        expiry: None,
                    },
                    last_change: None,
                },
                handled: false,
            },
        ];

        let r_hosts: Vec<crate::types::icingaweb::Host> =
            serde_json::from_slice(HOST_RESP).unwrap();
        let r: Result<Vec<Host>, String> = r_hosts.into_iter().map(|h| Host::try_from(h)).collect();
        assert_eq!(r, Ok(hosts))
    }

    #[test]
    fn test_de_services() {
        let services: Vec<Service> = vec![
            Service {
                name: "localhost!Disk".into(),
                display_name: "Disk".into(),
                host_name: "localhost".into(),
                state: ServiceState::CRITICAL,
                last_state: ServiceState::CRITICAL,
                last_hard_state: ServiceState::CRITICAL,
                last_state_change: Timestamp::new(1651294826 as f64),
                next_check: Timestamp::new(1651300378.99998),
                last_check_result: Some(LastCheckResult{ output: "DISK CRITICAL - free space: /etc/resolv.conf 17150 MB (69% inode=91%); /etc/hostname 17150 MB (69% inode=91%); /etc/hosts 17150 MB (69% inode=91%);".into()}),
                acknowledgement: Acknowledgement::none(),
                handled: false,

                last_state_critical: Timestamp::zero(),
                last_state_warning: Timestamp::zero(),
                last_state_ok: Timestamp::zero(),
                last_state_unknown: Timestamp::zero(),
                last_reachable: true,
            },
            Service {
                name: "exchange.icinga.com!HTTP".into(),
                display_name: "HTTP".into(),
                host_name: "exchange.icinga.com".into(),
                state: ServiceState::CRITICAL,
                last_state: ServiceState::CRITICAL,
                last_hard_state: ServiceState::CRITICAL,
                last_state_change: Timestamp::new(1651294824 as f64),
                next_check: Timestamp::new(1651300376.99998),
                last_check_result: Some(LastCheckResult{ output: "HTTP CRITICAL: HTTP/1.1 200 OK - string 'Welcome to Icinga Exchange' not found on 'http://exchange.icinga.com:80/' - 11176 bytes in 0.007 second response time ".into()}),
                acknowledgement: Acknowledgement::none(),
                handled: true,

                last_state_critical: Timestamp::zero(),
                last_state_warning: Timestamp::zero(),
                last_state_ok: Timestamp::zero(),
                last_state_unknown: Timestamp::zero(),
                last_reachable: true,
            },
            Service {
                name: "git.icinga.com!SSH".into(),
                display_name: "SSH".into(),
                host_name: "git.icinga.com".into(),
                state: ServiceState::OK,
                last_state: ServiceState::OK,
                last_hard_state: ServiceState::OK,
                last_state_change: Timestamp::new(1651294824 as f64),
                next_check: Timestamp::new(1651300378.99998),
                last_check_result: Some(LastCheckResult{ output: "SSH OK - OpenSSH_8.2p1 Ubuntu-4ubuntu0.4 (protocol 2.0) ".into()}),
                acknowledgement: Acknowledgement::none(),
                handled: true,

                last_state_critical: Timestamp::zero(),
                last_state_warning: Timestamp::zero(),
                last_state_ok: Timestamp::zero(),
                last_state_unknown: Timestamp::zero(),
                last_reachable: true,
            },
            Service {
                name: "localhost!Swap".into(),
                display_name: "Swap".into(),
                host_name: "localhost".into(),
                state: ServiceState::CRITICAL,
                last_state: ServiceState::CRITICAL,
                last_hard_state: ServiceState::CRITICAL,
                last_state_change: Timestamp::new(1651294823 as f64),
                next_check: Timestamp::new(1651300377.99998),
                last_check_result: Some(LastCheckResult{ output: "SWAP CRITICAL - 0% free (0 MB out of 0 MB) - Swap is either disabled, not present, or of zero size. ".into()}),
                acknowledgement: Acknowledgement { state: AckState::Acknowledged { kind: AckKind::Normal, expiry: None }, last_change: None },
                handled: true,
                last_state_critical: Timestamp::zero(),
                last_state_warning: Timestamp::zero(),
                last_state_ok: Timestamp::zero(),
                last_state_unknown: Timestamp::zero(),
                last_reachable: true,
            },
            Service {
                name: "srv-dns1.icinga.com!DNS".into(),
                display_name: "DNS".into(),
                host_name: "srv-dns1.icinga.com".into(),
                state: ServiceState::OK,
                last_state: ServiceState::OK,
                last_hard_state: ServiceState::OK,
                last_state_change: Timestamp::new(1651300890 as f64),
                next_check: Timestamp::new(1651304055.99998),
                last_check_result: Some(LastCheckResult{ output: "DNS OK: 0.019 seconds response time. srv-dns1.icinga.com returns 185.11.252.53".into()}),
                acknowledgement: Acknowledgement::none(),
                handled: false,

                last_state_critical: Timestamp::zero(),
                last_state_warning: Timestamp::zero(),
                last_state_ok: Timestamp::zero(),
                last_state_unknown: Timestamp::zero(),
                last_reachable: true,
            }

        ];

        let r_services: Vec<crate::types::icingaweb::Service> =
            serde_json::from_slice(SERVICE_RESP).unwrap();
        let r: Result<Vec<Service>, String> = r_services
            .into_iter()
            .map(|s| Service::try_from(s))
            .collect();
        assert_eq!(r, Ok(services))
    }
}
