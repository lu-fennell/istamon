lua << EOF
  local nvim_lsp = require('lspconfig')

  nvim_lsp.rust_analyzer.setup {
    settings = {
      ["rust-analyzer"] = {
        cargo = {
          features = {"cli"}
          }
        }
      }
    }

EOF
