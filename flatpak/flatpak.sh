#!/bin/bash

set -e

flatpak-builder --user --repo=repo --force-clean --install-deps-from=flathub build de.lu_fennell.istamon.yml
flatpak build-bundle repo istamon.flatpak de.lu_fennell.istamon --runtime-repo=https://flathub.org/repo/flathub.flatpakrepo
